﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using ADI.PDB.Data.Entity;
using ADI.PDB.Data;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

namespace ADI.PDB.ModelService.Services
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ModelService : IModelService
    {
        #region IModelService Members

        /// <summary>
        /// Gets the PCNPDN update count for given products.
        /// </summary>
        /// <param name="generics">The generics.</param>
        /// <param name="duration">The duration.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        public PCNPDNUpdateCount GetPCNPDNUpdateCount(List<string> generics, int duration, string format = "")
        {
            if (string.Equals(format, "xml", StringComparison.OrdinalIgnoreCase))
                WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;

            PCNPDNUpdateCount response= null;
            if (generics != null && generics.Count() > 0)
            {
                generics = generics.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
                response = PDBRepository.GetPCNPDNUpdateCount(generics, duration);
            }
            return response;
        }

        /// <summary>
        /// Gets a list of generic model maps
        /// </summary>
        /// <returns></returns>
        
        public IEnumerable<GenericModelMap> GetModels(string generics, string evals, string parameters = "", string format = "")
        {

            if (string.Equals(format, "xml", StringComparison.OrdinalIgnoreCase))
                WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;
            IEnumerable<GenericModelMap> data = null;

            List<string> genericsList = null;
            List<string> evalsList = null;
            List<string> paramList = null;

            List<string> evalsGenericsList = new List<string>();

            if (!string.IsNullOrEmpty(generics))
            {
                genericsList = generics.Split(',').ToList();
                genericsList = genericsList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            }
            if (!string.IsNullOrEmpty(evals))
            {
                evalsList = evals.Split(',').ToList();
                evalsList = evalsList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();

                //Added for AL-10441: Means to identify LTC Evaluation Boards
                foreach (var model in evalsList)
                {
                    IList<ModelGeneric> genericInfo = GetGenericInfo(model);

                    for (int count = 0; count < genericInfo.Count; count++)
                    {
                        evalsGenericsList.Add(genericInfo[count].GenericNumber);
                    }
                }
            }
            if (!string.IsNullOrEmpty(parameters))
            {
                paramList = parameters.Split(',').ToList();
                paramList = paramList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            }


            //If nothing is passed then return null
            if (genericsList == null && evalsList == null)
            {
                data = null;
            }
            else
            {
                data = PDBRepository.GetModels(genericsList, evalsList, paramList, evalsGenericsList, true);
            }
            return (data);
        }

        /// <summary>
        ///  This method is to get only the Models for generics or Evals
        /// </summary>
        /// <param name="generics"></param>
        /// <param name="evals"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public IEnumerable<GenericEvalModel> GetGenEvalModels(string generics, string evals, string format = "")
        {

            if (string.Equals(format, "xml", StringComparison.OrdinalIgnoreCase))
                WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;
            IEnumerable<GenericEvalModel> data = null;
            List<string> genericsList = null;
            List<string> evalsList = null;


            if (!string.IsNullOrEmpty(generics))
            {
                genericsList = generics.Split(',').ToList();
                genericsList = genericsList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            }
            if (!string.IsNullOrEmpty(evals))
            {
                evalsList = evals.Split(',').ToList();
                evalsList = evalsList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            }

            //If nothing is passed then return null
            if (genericsList == null && evalsList == null)
            {
                data = null;
            }
            else
            {
                data = PDBRepository.GetGenEvalModels(genericsList, evalsList);
            }
            return (data);


        }


        /// <summary>
        /// Gets dsitributor stock avaialability 
        /// </summary>
        /// <returns></returns>
        public DistiModelBundle GetDistiInventory(string models, string countrycode, string format = "")
        {
            if (string.Equals(format, "xml", StringComparison.OrdinalIgnoreCase))
                WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;


            List<string> modelsList = models.Split(',').ToList();
            modelsList = modelsList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            DistiModelBundle data = PDBRepository.GetDistiModelAvailability(modelsList, countrycode);
            return (data);
        }

        /// <summary>
        /// Gets Distributor Stock Availability Post
        /// </summary>
        public DistiModelBundle GetDistiInventoryPost(ModelDistributor modelDistributor)
        {
            if (modelDistributor == null || modelDistributor.Models == null || modelDistributor.Models.Count <= 0)
                return null;

            modelDistributor.Models = modelDistributor.Models.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            DistiModelBundle data = PDBRepository.GetDistiModelAvailability(modelDistributor.Models, modelDistributor.CountryCode);
            return data;
        }

        /// <summary>
        /// Gets generics life cycle
        /// </summary>
        /// <param name="generics">Generics as CSV</param>
        /// <param name="format">output format(xml or json)</param>
        /// <returns></returns>
        public IEnumerable<GenericInfo> GetProductLifeCycle(string generics, string format = "")
        {

            if (string.Equals(format, "xml", StringComparison.OrdinalIgnoreCase))
                WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;
            List<string> genericsList = generics.Split(',').ToList();
            genericsList = genericsList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            IEnumerable<GenericInfo> data = PDBRepository.GetGenericLiftCycle(genericsList);
            return (data);
        }

        public GenericParameters GetGenericParameterInfo(string generics, string format = "")
        {
            if (string.Equals(format, "xml", StringComparison.OrdinalIgnoreCase))
                WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;
            List<string> genericsList = generics.Split(',').ToList();
            genericsList = genericsList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            GenericParameters data = PDBRepository.GetGenericParameters(genericsList);
            return (data);
        }

        public IEnumerable<GenericInfo> GetGenParameterListandValue(string generics, string parameters, string format = "")
        {
            if (string.Equals(format, "xml", StringComparison.OrdinalIgnoreCase))
                WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;
            List<string> genericsList = generics.Split(',').ToList();
            List<string> paramsList = parameters.Split(',').ToList();
            genericsList = genericsList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            paramsList = paramsList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            IEnumerable<GenericInfo> data = PDBRepository.GetGenParamsListandValue(genericsList, paramsList);
            return data;
        }


        /// <summary>
        /// Get PCN/PDN info for models
        /// </summary>
        /// <param name="models">Models as CSV</param>
        /// <param name="format">output format(xml or json)</param>
        /// <returns></returns>
        public IEnumerable<ModelPCNPDNMap> GetPCNPDN(string models, string format = "")
        {
            if (string.Equals(format, "xml", StringComparison.OrdinalIgnoreCase))
                WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;


            List<string> modelsList = models.Split(',').ToList();
            modelsList = modelsList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            IEnumerable<ModelPCNPDNMap> data = PDBRepository.GetPCNPDNForModelsMap(modelsList);
            return (data);
        }

        /// <summary>
        /// Get Price, package and Availability for generics
        /// </summary>
        /// <param name="generics">Generics as CSV</param>
        /// <param name="countrycode">Country code</param>
        /// <param name="format">output format(xml or json)</param>
        /// <returns></returns>
        public GenericModelDistiByCountry GetPPA(string generics, string countrycode, string parameters = "", string format = "")
        {
            if (string.Equals(format, "xml", StringComparison.OrdinalIgnoreCase))
                WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;
            List<string> paramList = null;
            if (!string.IsNullOrEmpty(parameters))
            {
                paramList = parameters.Split(',').ToList();
                paramList = paramList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            }
            List<string> genericsList = generics.Split(',').ToList();
            genericsList = genericsList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            GenericModelDistiByCountry data = PDBRepository.GetPPABundle(genericsList, countrycode, paramList);
            return (data);
        }

        /// <summary>
        /// Get Models By Id
        /// </summary>
        /// <param name="models">models as CSV</param>
        /// <param name="format">output format(xml or json)</param>
        /// <returns></returns>
        public IList<Models> GetModelsById(string models, string parameters = "", string format = "")
        {

            if (string.Equals(format, "xml", StringComparison.OrdinalIgnoreCase))
                WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;

            List<string> paramList = null;
            if (!string.IsNullOrEmpty(parameters))
            {
                paramList = parameters.Split(',').ToList();
                paramList = paramList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            }
            List<string> modelsList = models.Split(',').ToList();
            modelsList = modelsList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            IList<Models> data = PDBRepository.GetModelsInfoById(modelsList, paramList);
            return (data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="models"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public IList<ModelGeneric> GetModelsGeneircInfo(string models, string format = "")
        {
            if (string.Equals(format, "xml", StringComparison.OrdinalIgnoreCase))
                WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;

            List<string> modelsList = models.Split(',').ToList();
            modelsList = modelsList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            IList<ModelGeneric> data = PDBRepository.GetModelsGeneric(modelsList);
            return data;
        }

        ///<summary>
        /// following stub should return product categories with PSTIDs
        /// </summary>
        /// <param name="cmsCatId"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public IList<CMSCategoryPST> GetCategoryPST(string cmsCatId = null, string format = "")
        {
            if (string.Equals(format, "xml", StringComparison.OrdinalIgnoreCase))
                WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;

            IList<CMSCategoryPST> objProductCategoryPSTId = PDBRepository.GetCategoryPSTRelation(cmsCatId);
            return objProductCategoryPSTId;
        }

        /// <summary>
        /// Method to get Generic Info
        /// </summary>
        /// <param name="models">Represents models</param>
        /// <param name="format">Represents Format</param>
        /// <returns></returns>
        public IList<ModelGeneric> GetGenericInfo(string models, string format = "")
        {
            if (string.Equals(format, "xml", StringComparison.OrdinalIgnoreCase))
                WebOperationContext.Current.OutgoingResponse.Format = WebMessageFormat.Xml;

            List<string> modelsList = models.Split(',').ToList();
            modelsList = modelsList.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            IList<ModelGeneric> data = PDBRepository.GetGeneric(modelsList);
            return data;
        }
        #endregion
    }
}