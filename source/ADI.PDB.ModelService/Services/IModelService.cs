﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using ADI.PDB.Data.Entity;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using ADI.PDB.Data;

namespace ADI.PDB.ModelService.Services
{
    [ServiceContract]
    public interface IModelService
    {
        /// <summary>
        /// Gets the models.
        /// </summary>
        /// <param name="generics">The generics.</param>
        /// <param name="evals">The evals.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetModels?generics={generics}&evals={evals}&parameters={parameters}&format={format}")]
        IEnumerable<GenericModelMap> GetModels(string generics, string evals, string parameters = "", string format = "");

        /// <summary>
        /// Gets the PCNPDN update count for given products.
        /// </summary>
        /// <param name="generics">The generics.</param>
        /// <param name="duration">The duration.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(UriTemplate = "/GetPCNPDNUpdateCount?duration={duration}&format={format}", Method = "POST")]
        PCNPDNUpdateCount GetPCNPDNUpdateCount(List<string> generics, int duration,string format = "");

        /// <summary>
        /// Gets the disti inventory.
        /// </summary>
        /// <param name="models">The models.</param>
        /// <param name="countrycode">The countrycode.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetDistiInventory?models={models}&countrycode={countrycode}&format={format}")]
        DistiModelBundle GetDistiInventory(string models, string countrycode, string format = "");

        /// <summary>
        /// Gets the disti inventory post.
        /// </summary>
        /// <param name="modelDistributor">The model distributor.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        DistiModelBundle GetDistiInventoryPost(ModelDistributor modelDistributor);


        /// <summary>
        /// Gets the product life cycle.
        /// </summary>
        /// <param name="generics">The generics.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetProductLifeCycle?generics={generics}&format={format}")]
        IEnumerable<GenericInfo> GetProductLifeCycle(string generics, string format = "");

        /// <summary>
        /// Gets the generic parameter information.
        /// </summary>
        /// <param name="generics">The generics.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetGenericParameterInfo?generics={generics}&format={format}")]
        GenericParameters GetGenericParameterInfo(string generics, string format = "");

        /// <summary>
        /// Gets the gen parameter listand value.
        /// </summary>
        /// <param name="generics">The generics.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetGenParameterListandValue?generics={generics}&parameters={parameters}&format={format}")]
        IEnumerable<GenericInfo> GetGenParameterListandValue(string generics, string parameters, string format = "");

        /// <summary>
        /// Gets the PCNPDN.
        /// </summary>
        /// <param name="models">The models.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetPCNPDN?models={models}&format={format}")]
        IEnumerable<ModelPCNPDNMap> GetPCNPDN(string models, string format = "");

        /// <summary>
        /// Gets the ppa.
        /// </summary>
        /// <param name="generics">The generics.</param>
        /// <param name="countrycode">The countrycode.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetPPA?generics={generics}&countrycode={countrycode}&parameters={parameters}&format={format}")]
        GenericModelDistiByCountry GetPPA(string generics, string countrycode, string parameters = "", string format = "");

        /// <summary>
        /// Gets the models by identifier.
        /// </summary>
        /// <param name="models">The models.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetModelsById?models={models}&parameters={parameters}&format={format}")]
        IList<Models> GetModelsById(string models, string parameters = "", string format = "");

        /// <summary>
        /// Gets the models geneirc information.
        /// </summary>
        /// <param name="models">The models.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetModelsGeneircInfo?models={models}&format={format}")]
        IList<ModelGeneric> GetModelsGeneircInfo(string models, string format = "");

        /// <summary>
        /// Gets the gen eval models.
        /// </summary>
        /// <param name="generics">The generics.</param>
        /// <param name="evals">The evals.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetGenEvalModels?generics={generics}&evals={evals}&format={format}")]
        IEnumerable<GenericEvalModel> GetGenEvalModels(string generics, string evals, string format = "");

        /// <summary>
        /// Gets the category PST.
        /// </summary>
        /// <param name="cmsCatId">The CMS cat identifier.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetCategoryPST?cmsCatId={cmsCatId}&format={format}")]
        IList<CMSCategoryPST> GetCategoryPST(string cmsCatId = null, string format = "");

        /// <summary>
        /// Gets the generic information.
        /// </summary>
        /// <param name="models">The models.</param>
        /// <param name="format">The format.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/GetGenericInfo?models={models}&format={format}")]
        IList<ModelGeneric> GetGenericInfo(string models, string format = "");
    }
}