﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ADI.PDB.Data.Entity;
using System.Configuration;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Threading.Tasks;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace ADI.PDB.Data
{
    public class PDBRepository
    {
        #region public method

        /// <summary>
        /// Gets the PCNPDN update count for given products.
        /// </summary>
        /// <param name="genericsList">The generics list.</param>
        /// <param name="duration">The duration.</param>
        /// <returns></returns>
        public static PCNPDNUpdateCount GetPCNPDNUpdateCount(List<string> genericsList, int duration)
        {
            Stopwatch stopwatch = new Stopwatch();
            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
                stopwatch.Start();
            var objPCNPDNUpdateCount = new PCNPDNUpdateCount();
            try
            {
                //Get the list of models
                var productXML = GenerateXMLString(genericsList, "l", "g", "id");
                //Build generic xml
                var models = GetGenericModels(productXML);
                var lstModels = models.Select(p => p.ModelNumber).ToList();
                //Build model xml
                var modelXML = GenerateXMLString(lstModels, "l", "m", "id");

                //Get PCN/PDN updates
                var pcnPDNUpdates = GetPCNPDNUpdateForModels(modelXML, duration);

                //Do the grouping of products and their pcnpdn count
                //Stopwatch stopwatchl = new Stopwatch();
                //stopwatchl.Start();
                var temp1 = from m in models
                            join p in pcnPDNUpdates on m.ModelNumber equals p.modelnumber
                            group new { m, p } by new { m.Generic } into g
                            select new
                            {
                                product = g.Key.Generic,
                                count = g.Select(x => x.p).Count()
                            };

                var temp2 = temp1.OrderBy(p => p.product).ToDictionary(item => item.product, item => item.count);
                //stopwatchl.Stop();
                //LogMessage("linqSec = " + stopwatchl.Elapsed.TotalSeconds, TraceEventType.Information);

                #region use it for debuging or trouble shooting the linq query return above
                /*Stopwatch stopwatchf = new Stopwatch();
                stopwatchf.Start();
                Dictionary<string, int> dictProductUpdateCount = new Dictionary<string, int>();
                Parallel.ForEach(genericsList, (generic) =>
                {
                    var tempModles = models.Where(p => p.Generic.Equals(generic, StringComparison.InvariantCultureIgnoreCase)).Select(p => p.ModelNumber).ToList();
                    var tempPCNPDN = pcnPDNUpdates.Where(p => tempModles.Contains(p.modelnumber)).Select(p => p).ToList();
                    dictProductUpdateCount.Add(generic, tempPCNPDN.Count());
                });
                dictProductUpdateCount = dictProductUpdateCount.OrderBy(p => p.Key).ToDictionary(item => item.Key, item => item.Value);
                stopwatchf.Stop();
                LogMessage("fSec =  " + stopwatchf.Elapsed.TotalSeconds, TraceEventType.Information);*/
                #endregion

                objPCNPDNUpdateCount.Duration = duration;
                objPCNPDNUpdateCount.ProductPCNPDNUpdateCount = temp2;
                objPCNPDNUpdateCount.TotalCount = pcnPDNUpdates.Count();
            }
            catch (Exception ex)
            {
                LogMessage("Error inside GetPCNPDNUpdateCount method.", TraceEventType.Error, ex);
            }
            finally
            {
                if (getConfigValue("EnableResponseTarce").ToLower() == "true")
                {
                    stopwatch.Stop();
                    LogMessage("GetPCNPDNUpdateCount Method Response time for " + genericsList.Count() + " generics and duration " + duration + " day " + stopwatch.Elapsed.TotalSeconds + " second(s).", TraceEventType.Information);
                }
            }
            return objPCNPDNUpdateCount;
        }



        /// <summary>
        /// Returns a list of GenericModelMap objects
        /// This method work with alias, example 3B01 which is a alias and 3B01G is generic
        /// </summary>
        /// <param name="lstGenerics">List of generics</param>
        /// <param name="includePCNPDN">Whether to include PCN/PDN info in output</param>
        /// <returns></returns>
        
        public static IEnumerable<GenericModelMap> GetModels(List<string> lstGenerics, List<string> lstEvals, List<string> lstParams, List<string> lstEvalGenerics, bool includePCNPDN = false)
        {
            Stopwatch stopwatch = new Stopwatch();
            lstParams = ValidatParameterList(lstParams);
            List<GenericModelMap> genericModelMaps = new List<GenericModelMap>();
            //if tracking is enabled then log the response time

            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
            {
                stopwatch.Start();
            }

            #region Generics processing

            if (lstGenerics != null)
            {
                //Remove null/empty string from the list
                lstGenerics.RemoveAll(a => string.IsNullOrEmpty(a));

                try
                {
                    string genericInfoParametersXML = GenerateXMLString(GetGenricInfoParameter(), "l", "p", "id");

                    string genericsXML = GenerateXMLString(lstGenerics, "l", "g", "id");

                    //Get symbol information for generics (Legacy parts)
                    IEnumerable<Adisp_GetLTCSymbolInformation_Result> enumLTCSymbolInformation = GetSymbolInfoForGenerics(genericsXML);


                    //Get generics status and vintage date from database for all generics
                    IEnumerable<CTGenericInformation> enumGenericsInfo = GetGenericInfo(genericsXML, genericInfoParametersXML);

                    List<string> lstTempGeneric = new List<string>();
                    lstTempGeneric = lstGenerics.Select(p => p).ToList();
                    foreach (string generic in lstGenerics)
                    {
                        var temp = enumGenericsInfo.Where(p => string.Equals(p.GenericNumber, generic, StringComparison.OrdinalIgnoreCase) || string.Equals(p.Alias, generic, StringComparison.OrdinalIgnoreCase)).Select(p => p);
                        if (temp.Count() > 0)
                        {
                            if (!string.IsNullOrEmpty(temp.FirstOrDefault().Alias))
                            {
                                lstTempGeneric.Remove(generic);
                                lstTempGeneric.Add(temp.FirstOrDefault().GenericNumber);
                            }
                        }
                    }

                    genericsXML = GenerateXMLString(lstTempGeneric, "l", "g", "id");


                    //Gets the list of all models and parameters for all required generics from PDB
                    IEnumerable<ModelParMap> enumGenericsModelsInfo = GetGenericModelsInfo(genericsXML, "");

                    if (enumGenericsModelsInfo.Count() > 0)
                    {

                        foreach (string generic in lstGenerics)
                        {
                            try
                            {
                                List<ModelParMap> lstGenericsModelParMap = null;
                                //Get the current processing generic information from list of all generics info(lstGenericsInfo)
                                List<CTGenericInformation> lstGenericInfo = FilterGenericInfoForGeneric(generic, enumGenericsInfo);

                                if (lstGenericInfo.Count() > 0)
                                {
                                    if (string.Equals(lstGenericInfo.FirstOrDefault().Alias, generic, StringComparison.OrdinalIgnoreCase))
                                        lstGenericsModelParMap = enumGenericsModelsInfo.Where(p => p.Generic.ToLower().Trim() == lstGenericInfo.FirstOrDefault().GenericNumber.ToLower().Trim()).Select(p => p).ToList();
                                    else
                                        lstGenericsModelParMap = enumGenericsModelsInfo.Where(p => p.Generic.ToLower().Trim() == generic.ToLower().Trim()).Select(p => p).ToList();
                                }
                                else
                                {
                                    lstGenericsModelParMap = enumGenericsModelsInfo.Where(p => p.Generic.ToLower().Trim() == generic.ToLower().Trim()).Select(p => p).ToList();
                                }

                                //Get the models information for current processing generics from enumGenericsModelsInfo list


                                genericModelMaps.Add(GetModelsByGenericsOrEvals(generic, lstParams, lstGenericsModelParMap, enumLTCSymbolInformation, lstGenericInfo, includePCNPDN));

                            }
                            catch (Exception ex)
                            {
                                LogMessage("There is issue while building GenericModelMap object for generic #-" + generic + ". ", TraceEventType.Error, ex);
                            }
                        }
                    }
                }
                catch (Exception exDb)
                {
                    LogMessage("Unable to get data from database.", TraceEventType.Error, exDb);
                }
            }
            #endregion

            #region Eval Boards processing
            if (lstEvals != null)
            {
                //Remove null/empty string from the list
                lstEvalGenerics.RemoveAll(a => string.IsNullOrEmpty(a));

                List<CTGenericInformation> lstEvalGenericInfo = new List<CTGenericInformation>();

                try
                {
                    string evalsXML = GenerateXMLString(lstEvals, "l", "g", "id");

                    //Gets the list of all models and parameters for all required generics from PDB
                    IEnumerable<ModelParMap> enumEvalsModelsInfo = GetEvalModelsInfo(evalsXML, "");

                    if (lstEvalGenerics != null && lstEvalGenerics.Any())
                    {
                        string evalGenericInfoParametersXML = GenerateXMLString(GetGenricInfoParameter(), "l", "p", "id");

                        string evalGenericsXML = GenerateXMLString(lstEvalGenerics, "l", "g", "id");

                        //Get generics status and vintage date from database for all generics
                        IEnumerable<CTGenericInformation> enumEvalGenericsInfo = GetGenericInfo(evalGenericsXML, evalGenericInfoParametersXML);

                        foreach (var evalGeneric in lstEvalGenerics)
                        {
                            //Get the current processing generic information from list of all generics info(lstEvalGenerics)
                            lstEvalGenericInfo = FilterGenericInfoForGeneric(evalGeneric, enumEvalGenericsInfo);
                        }
                    }

                    if (enumEvalsModelsInfo.Count() > 0)
                    {
                        foreach (string evals in lstEvals)
                        {
                            try
                            {
                                //Get the models information for current processing Evals from enumEvalsModelsInfo list
                                List<ModelParMap> lstGenericsModelParMap = enumEvalsModelsInfo.Where(p => p.Generic.ToLower().Trim() == evals.ToLower().Trim()).Select(p => p).ToList();

                                genericModelMaps.Add(GetModelsByGenericsOrEvals(evals, lstParams, lstGenericsModelParMap, null, lstEvalGenericInfo, false));
                            }
                            catch (Exception ex)
                            {
                                LogMessage("There is issue while building GenericModelMap object for Eval #-" + evals + ". ", TraceEventType.Error, ex);
                            }
                        }
                    }
                }
                catch (Exception exDb)
                {
                    LogMessage("Unable ot get data from database.", TraceEventType.Error, exDb);
                }
            }
            #endregion

            #region Adds BuyId For Matching Model Names
            //Gets the BuyId for All the Applicable Models
            if (genericModelMaps != null && genericModelMaps.Count > 0)
            {
                foreach (var item in genericModelMaps)
                {
                    IEnumerable<ADISP_GetBuyIdByModels_Result> lstModelsAndBuyIds = null;
                    var modelNameList = item.Models != null && item.Models.Count > 0 ? item.Models.Select(x => x.ModelNumber) : null;
                    if (modelNameList != null && modelNameList.Any())
                    {
                        string modelsNameXML = GenerateXMLString(modelNameList, "l", "p", "id");
                        lstModelsAndBuyIds = GetModelsAndBuyIds(modelsNameXML);

                        if (lstModelsAndBuyIds != null && lstModelsAndBuyIds.Any())
                        {
                            foreach (var currentModel in item.Models)
                            {
                                var selectedModel = lstModelsAndBuyIds.Where(x => x.Model_Name.Equals(currentModel.ModelNumber, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                                currentModel.BuyId = selectedModel != null ? selectedModel.Buy_Id ?? 0 : 0;
                                currentModel.Quantity = selectedModel != null ? selectedModel.Quantity ?? 0 : 0;
                            }
                        }
                    }
                }
            }
            #endregion

            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
            {
                stopwatch.Stop();
                string genericCSV = string.Empty;
                string evalCSV = string.Empty;
                if (lstGenerics != null)
                {
                    genericCSV = String.Join(",", lstGenerics.Select(x => x.ToString()).ToArray());

                }
                if (lstEvals != null)
                {
                    evalCSV = String.Join(",", lstEvals.Select(x => x.ToString()).ToArray()); ;
                }
                LogMessage("GetModels Method Response time for generics : '" + genericCSV + "' and Evals : '" + evalCSV + "' is " + stopwatch.Elapsed.TotalSeconds + " second(s).", TraceEventType.Information);
            }
            return (genericModelMaps);
        }

        public static IEnumerable<GenericEvalModel> GetGenEvalModels(List<string> lstGenerics, List<string> lstEvals)
        {
            List<GenericEvalModel> genericModelMaps = new List<GenericEvalModel>();
            #region Generics
            if (lstGenerics != null)
            {
                //Remove null/empty string from the list
                lstGenerics.RemoveAll(a => string.IsNullOrEmpty(a));

                try
                {

                    string genericInfoParametersXML = GenerateXMLString(GetGenricInfoParameter(), "l", "p", "id");

                    string genericsXML = GenerateXMLString(lstGenerics, "l", "g", "id");

                    //Get generics status and vintage date from database for all generics
                    IEnumerable<CTGenericInformation> enumGenericsInfo = GetGenericInfo(genericsXML, genericInfoParametersXML);

                    List<string> lstTempGeneric = new List<string>();
                    lstTempGeneric = lstGenerics.Select(p => p).ToList();
                    foreach (string generic in lstGenerics)
                    {
                        var temp = enumGenericsInfo.Where(p => string.Equals(p.GenericNumber, generic, StringComparison.OrdinalIgnoreCase) || string.Equals(p.Alias, generic, StringComparison.OrdinalIgnoreCase)).Select(p => p);
                        if (temp.Count() > 0)
                        {
                            if (!string.IsNullOrEmpty(temp.FirstOrDefault().Alias))
                            {
                                lstTempGeneric.Remove(generic);
                                lstTempGeneric.Add(temp.FirstOrDefault().GenericNumber);
                            }
                        }
                    }

                    genericsXML = GenerateXMLString(lstTempGeneric, "l", "g", "id");


                    //Gets the list of all models and parameters for all required generics from PDB
                    IEnumerable<ModelParMap> enumGenericsModelsInfo = GetGenericModelsInfo(genericsXML, "");

                    if (enumGenericsModelsInfo.Count() > 0)
                    {

                        foreach (string generic in lstGenerics)
                        {
                            try
                            {
                                List<ModelParMap> lstGenericsModelParMap = null;
                                //Get the current processing generic information from list of all generics info(lstGenericsInfo)
                                List<CTGenericInformation> lstGenericInfo = FilterGenericInfoForGeneric(generic, enumGenericsInfo);

                                if (lstGenericInfo.Count() > 0)
                                {
                                    if (string.Equals(lstGenericInfo.FirstOrDefault().Alias, generic, StringComparison.OrdinalIgnoreCase))
                                        lstGenericsModelParMap = enumGenericsModelsInfo.Where(p => p.Generic.ToLower().Trim() == lstGenericInfo.FirstOrDefault().GenericNumber.ToLower().Trim()).Select(p => p).ToList();
                                    else
                                        lstGenericsModelParMap = enumGenericsModelsInfo.Where(p => p.Generic.ToLower().Trim() == generic.ToLower().Trim()).Select(p => p).ToList();
                                }
                                else
                                {
                                    lstGenericsModelParMap = enumGenericsModelsInfo.Where(p => p.Generic.ToLower().Trim() == generic.ToLower().Trim()).Select(p => p).ToList();
                                }

                                //Get the models information for current processing generics from enumGenericsModelsInfo list


                                genericModelMaps.Add(GetGenericsOrEvalModels(generic, null, lstGenericsModelParMap, lstGenericInfo));
                            }
                            catch (Exception ex)
                            {
                                Logger.Write(ex.StackTrace);
                            }
                        }
                    }
                }
                catch (Exception exDb)
                {
                    LogMessage("Unable ot get data from database.", TraceEventType.Error, exDb);
                }
            }
            #endregion


            if (lstEvals != null)
            {
                //Remove null/empty string from the list
                string evalsXML = GenerateXMLString(lstEvals, "l", "g", "id");

                try
                {
                    //Gets the list of all models and parameters for all required generics from PDB
                    IEnumerable<ModelParMap> enumEvalsModelsInfo = GetEvalModelsInfo(evalsXML, "");

                    if (enumEvalsModelsInfo.Count() > 0)
                    {
                        foreach (string evals in lstEvals)
                        {
                            try
                            {
                                //Get the models information for current processing Evals from enumEvalsModelsInfo list
                                List<ModelParMap> lstGenericsModelParMap = enumEvalsModelsInfo.Where(p => p.Generic.ToLower().Trim() == evals.ToLower().Trim()).Select(p => p).ToList();

                                genericModelMaps.Add(GetGenericsOrEvalModels(evals, null, lstGenericsModelParMap, null));
                            }
                            catch (Exception ex)
                            {
                                LogMessage("There is issue while building GenericModelMap object for Eval #-" + evals + ". ", TraceEventType.Error, ex);
                            }
                        }
                    }
                }
                catch (Exception exDb)
                {
                    LogMessage("Unable ot get data from database.", TraceEventType.Error, exDb);
                }
            }

            return (genericModelMaps);
        }



        /// <summary>
        /// Get Models Information By Id
        /// </summary>
        /// <param name="lstModels">List of models</param>
        /// <returns>List of model objects</returns>
        public static IList<Models> GetModelsInfoById(List<string> lstInputModels, List<string> lstParams)
        {
            lstParams = ValidatParameterList(lstParams);
            //if tracking is enabled then log the response time
            StopWatch watch = new StopWatch();
            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
                watch.Start();

            List<Models> retModels = new List<Models>();
            //Generate XML of models to pass as DB parameter
            string modelXML = string.Empty;
            modelXML = GenerateXMLString(lstInputModels, "l", "m", "id");

            //Get the model generic information
            IEnumerable<CTGenericInfoByModel> enumGenericInfoByModel = null;
            using (var pdbEntities = new PDBEntities())
            {
                enumGenericInfoByModel = (from m in pdbEntities.GetGenericInfoByModelXML(modelXML)
                                          select m).ToList();
            }
            IEnumerable<Adisp_GetLTCSymbolInformation_Result> enumLTCSymbolInformation = null;
            IEnumerable<CTGenericInformation> enumGenericsInfo = null;
            if (enumGenericInfoByModel != null && enumGenericInfoByModel.Any())
            {
                string genericInfoParametersXML = GenerateXMLString(GetGenricInfoParameter(), "l", "p", "id");
                string genericsXML = GenerateXMLString(enumGenericInfoByModel.Select(x => x.GenericNumber).ToList(), "l", "g", "id");
                //Get symbol information for generics (Legacy parts)
                enumLTCSymbolInformation = GetSymbolInfoForGenerics(genericsXML);
                //Get generics status and vintage date from database for all generics
                enumGenericsInfo = GetGenericInfo(genericsXML, genericInfoParametersXML);
            }
            try
            {
                using (var pdbEntities = new PDBEntities())
                {
                    List<RemovedProductMapping> lstModelPCNPDNRemoved = GetModelPCNPDNRemoved(lstInputModels);


                    //Gets the list of all model parameters for lstModels from PDB
                    IEnumerable<ModelParMap> enumModelparquery = (from map in pdbEntities.GetModelParMap(modelXML, "")
                                                                  select map).ToList();

                    //Get PCN/PDN information
                    IEnumerable<CTPCNPDNContent> enumPCNPDNContent = GetModelPCNPDNFromDB(modelXML);

                    //Get model symbols details from DB for generic
                    IEnumerable<CTModelSymbols> enumModelSymbols = GetModelSymbolsByGeneric(null);
                    foreach (var model in lstInputModels)
                    {
                        try
                        {
                            retModels.Add(GetModelObject(model, lstParams, lstModelPCNPDNRemoved, enumModelparquery, enumPCNPDNContent, enumModelSymbols, enumGenericInfoByModel, null, enumLTCSymbolInformation, enumGenericsInfo));
                        }
                        catch (Exception ex)
                        {
                            LogMessage("Error while building Models object for model # - " + model + ". ", TraceEventType.Error, ex);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LogMessage("Error while generating  models object for model # -" + String.Join(",", lstInputModels.Select(x => x.ToString()).ToArray()) + ". ", TraceEventType.Error, ex);
            }

            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
            {
                watch.Stop();
                LogMessage("GetModelsInfo Method Response time for '" + String.Join(",", lstInputModels.Select(x => x.ToString()).ToArray()) + "' : " + watch.GetElapsedTimeSecs().ToString() + " second(s).", TraceEventType.Information);
            }
            return retModels;


        }

        /// <summary>
        /// Get distributor and availability information
        /// </summary>
        /// <param name="lstModels"></param>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        public static DistiModelBundle GetDistiModelAvailability(List<string> lstModels, string countryCode)
        {
            //Remove null/empty string from the list
            lstModels.RemoveAll(a => string.IsNullOrEmpty(a));

            //if tracking is enabled then log the response time
            StopWatch watch = new StopWatch();
            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
                watch.Start();

            IEnumerable<CTDistiModelStockInfo> lstDistiModelStockInfo = null;
            DistiModelBundle objDistiModelMap = new DistiModelBundle();
            IList<DistiInfo> lstDistiInfo = new List<DistiInfo>();
            IList<DistiModel> lstDistiModel = new List<DistiModel>();
            try
            {
                //Generate XML of models to pass as DB call parameter
                string modelXML = string.Empty;

                modelXML = GenerateXMLString(lstModels, "l", "m", "id");

                using (var pdbEntities = new PDBEntities())
                {
                    lstDistiModelStockInfo = (from info in pdbEntities.GetDistiStockAvailabilityOfModelXML(modelXML, countryCode)
                                              select info).ToList();
                }
                objDistiModelMap.Country = countryCode;

                IEnumerable<CTModelAdiInventory> enumModelAdiInventory = GetModelAdiInventory(GenerateXMLString(lstModels, "l", "p", "id"), countryCode);

                #region get the distributors
                //Get the distinct disti ids from lstDistiModelStockInfo
                var lstDistinctDistis = (from disti in lstDistiModelStockInfo
                                         select disti.Disti_id).Distinct();

                //Get the disti info from lstDistiModelStockInfo for each disti id
                foreach (var disti in lstDistinctDistis)
                {

                    DistiInfo objDistiInfo = null;
                    try
                    {
                        objDistiInfo = (from distinfo in lstDistiModelStockInfo
                                        where distinfo.Disti_id.Equals(disti)
                                        select new DistiInfo
                                        {
                                            DistiID = distinfo.Disti_id,
                                            DistiName = distinfo.displayName,
                                            DistiHomeURL = distinfo.homeUrl
                                        }).FirstOrDefault();
                        if (lstDistiModelStockInfo.Any(x => x.Disti_id == 1034601))
                        {
                            if (countryCode.ToLower() == "jp" && lstDistiModelStockInfo.Any(x => x.displayName == "MOUSER"))
                            {
                                objDistiInfo = (from dist in lstDistiModelStockInfo
                                                where dist.Disti_id.Equals(disti)
                                                select new DistiInfo
                                                {
                                                    DistiID = dist.Disti_id,
                                                    DistiName = dist.displayName,
                                                    DistiHomeURL = dist.homeUrl

                                                }).FirstOrDefault();
                                lstDistiModelStockInfo.Where(c => c.Disti_id == 1034601).FirstOrDefault().displayName = "Macnica-Mouser";


                            }

                        }





                        lstDistiInfo.Add(objDistiInfo);



                    }
                    catch (Exception ex)
                    {
                        LogMessage("Error while building DistiInfo object for Disti # - " + disti.ToString() + ".", TraceEventType.Error, ex);
                    }
                }

                objDistiModelMap.Distributors = lstDistiInfo;
                #endregion

                #region Get the quantity for each models
                //Foreach model get the quantity, part url, disti info
                DistiModel objDistiModel = null;
                Dictionary<int, ModelDistiQuantity> dictModelDistiQuantity = null;
                foreach (var distinctModel in lstModels)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(distinctModel))
                        {
                            objDistiModel = new DistiModel();
                            //Get the model quantity info from lstDistiModelStockInfo list
                            var modelsQuantityInfo = from qtyInfo in lstDistiModelStockInfo
                                                     where qtyInfo.modelNumber == distinctModel
                                                     select qtyInfo;


                            ModelDistiQuantity objModelDistiQuantity = null;
                            dictModelDistiQuantity = new Dictionary<int, ModelDistiQuantity>();
                            //For each distributor under the modelsQuantityInfo build ModelDistiQuantity object
                            foreach (var modelQuantity in modelsQuantityInfo)
                            {
                                objModelDistiQuantity = new ModelDistiQuantity();
                                objModelDistiQuantity.Quantity = Convert.ToInt32(modelQuantity.quantity);
                                objModelDistiQuantity.PartURL = modelQuantity.url.ToString();

                                if (objModelDistiQuantity.PartURL.Contains("http://www.mouser.com") && countryCode.ToLower() == "jp")
                                {
                                    objModelDistiQuantity.PartURL = objModelDistiQuantity.PartURL.Replace("http://www.mouser.com", "http://www.macnica-mouser.jp");
                                }
                                dictModelDistiQuantity.Add(modelQuantity.Disti_id, objModelDistiQuantity);
                            }
                            objDistiModel.ModelNumber = distinctModel;
                            objDistiModel.ModelDistributors = dictModelDistiQuantity;
                            objDistiModel.ModelInventory = GetModelAdiInventoryFromList(distinctModel, enumModelAdiInventory);
                            lstDistiModel.Add(objDistiModel);
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMessage("Error while building DistiModel object for Model # - " + distinctModel + ".", TraceEventType.Error, ex);
                    }
                }
                objDistiModelMap.Models = lstDistiModel;
                #endregion
            }
            catch (Exception ex)
            {
                LogMessage("Error in GetDistiModelAvailability methode.", TraceEventType.Error, ex);
            }
            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
            {
                watch.Stop();
                LogMessage("GetDistiInventory Method Response time for '" + String.Join(",", lstModels.Select(x => x.ToString()).ToArray()) + "' : " + watch.GetElapsedTimeSecs().ToString() + " second(s).", TraceEventType.Information);
            }
            return objDistiModelMap;
        }

        /// <summary>
        /// Gets generics life cycle
        /// This method work with alias, example 3B01 which is a alias and 3B01G is generic
        /// </summary>
        /// <param name="lstGenerics">List of generics</param>
        /// <returns></returns>
        public static IEnumerable<GenericInfo> GetGenericLiftCycle(List<string> lstGenerics)
        {
            //Remove null/empty string from the list
            lstGenerics.RemoveAll(a => string.IsNullOrEmpty(a));

            //if tracking is enabled then log the response time
            StopWatch watch = new StopWatch();
            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
                watch.Start();

            List<GenericInfo> genericInfo = new List<GenericInfo>();
            string genericsXML = string.Empty;
            genericsXML = GenerateXMLString(lstGenerics, "l", "g", "id");

            string genericInfoParametersXML = GenerateXMLString(GetGenricInfoParameter(), "l", "p", "id");

            //Get generic status and vintage date from database
            IEnumerable<CTGenericInformation> lstGenericsInfo = GetGenericInfo(genericsXML, genericInfoParametersXML);

            foreach (string generic in lstGenerics)
            {
                List<CTGenericInformation> lstGenericInfo = new List<CTGenericInformation>();
                try
                {
                    //Get the current processing generic information from list of all generics info(lstGenericsInfo)
                    lstGenericInfo = FilterGenericInfoForGeneric(generic, lstGenericsInfo);

                    genericInfo.Add(GetGenericInfoObject(lstGenericInfo));
                }
                catch (Exception ex)
                {
                    LogMessage("There is issue while building GenericInfo object for generic #-" + generic + ". ", TraceEventType.Error, ex);
                }
            }
            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
            {
                watch.Stop();
                LogMessage("GetProductLifeCycle Method Response time for '" + String.Join(",", lstGenerics.Select(x => x.ToString()).ToArray()) + "' : " + watch.GetElapsedTimeSecs().ToString() + " second(s).", TraceEventType.Information);
            }
            return genericInfo;
        }

        public static GenericParameters GetGenericParameters(List<string> lstGenerics)
        {
            //Remove null/empty string from the list
            lstGenerics.RemoveAll(a => string.IsNullOrEmpty(a));

            GenericParameters genericInfo = new GenericParameters();

            string genericsXML = string.Empty;
            genericsXML = GenerateXMLString(lstGenerics, "l", "g", "id");


            //Get generic status and vintage date from database
            List<Adisp_GetGenericParameterInformation_Result> lstGenericsInfo = GetGenParamInfo(genericsXML);
            if (lstGenericsInfo != null && lstGenericsInfo.Count() > 0)
            {
                var paramToRemove = lstGenericsInfo.SingleOrDefault(s => s.ParameterID == 1709);
                if (paramToRemove != null && paramToRemove.ParameterID != 0)
                {
                    lstGenericsInfo.Remove(paramToRemove);
                }
                genericInfo = GetGenParamInfoResult(lstGenericsInfo);

            }

            return genericInfo;
        }

        public static IEnumerable<GenericInfo> GetGenParamsListandValue(List<string> lstGenerics, List<string> lstParams)
        {

            lstGenerics.RemoveAll(a => string.IsNullOrEmpty(a));
            lstParams.RemoveAll(a => string.IsNullOrEmpty(a));


            List<GenericInfo> genericInfo = new List<GenericInfo>();
            string genericsXML = string.Empty;
            genericsXML = GenerateXMLString(lstGenerics, "l", "g", "id");

            string genericInfoParametersXML = GenerateXMLString(lstParams, "l", "p", "id");
            string genericBasicInforParamXml = GenerateXMLString(GetGenricInfoParameter(), "l", "p", "id");

            IEnumerable<CTGenericInformation> lstGenericsBasicInfo = null;
            IEnumerable<Adisp_GetGenericParameterListandValue1_Result> lstGenericsInfo = null;
            IEnumerable<ModelParMap> enumGenericsModelsInfo = null;


            var tasks = new List<Task>();

            tasks.Add(Task.Factory.StartNew(() =>
            lstGenericsBasicInfo = GetGenericInfo(genericsXML, genericBasicInforParamXml)));
            tasks.Add(Task.Factory.StartNew(() => lstGenericsInfo = GetGenParamListValue(genericsXML, genericInfoParametersXML)));
            tasks.Add(Task.Factory.StartNew(() => enumGenericsModelsInfo = GetGenericModelsInfo(genericsXML, "<l><p id=\"801\"/></l>").OrderBy(p => p.Value).Where(p => !p.Value.StartsWith("0"))));

            Task.WaitAll(tasks.ToArray());

            Parallel.ForEach(lstGenerics, (generic) =>

            //foreach (string generic in lstGenerics)
            {
                GenericInfo genInfo = new GenericInfo();

                List<Adisp_GetGenericParameterListandValue1_Result> lstGenericsModelParMap = lstGenericsInfo.Where(p => p.PartDisplayValue.ToLower().Trim() == generic.ToLower().Trim()).Select(p => p).ToList();
                List<CTGenericInformation> lstGeneric = FilterGenericInfoForGeneric(generic, lstGenericsBasicInfo);
                List<ModelParMap> test12 = enumGenericsModelsInfo.Where(p => p.Generic.ToLower().Trim() == generic.ToLower().Trim()).Select(p => p).ToList();



                if (lstGeneric != null && lstGeneric.Count > 0)
                {
                    if (GetGenericInfoObject(lstGeneric).Generic != null)
                        genInfo.Generic = GetGenericInfoObject(lstGeneric).Generic;
                    if (GetGenericInfoObject(lstGeneric).Status != null)
                        genInfo.Status = GetGenericInfoObject(lstGeneric).Status;

                    genInfo.GenericParameters = new List<GenericParams>();

                    if (lstGenericsModelParMap != null && lstGenericsModelParMap.Count > 0)
                    {

                        List<GenericParams> mpList = new List<GenericParams>();

                        foreach (var genInfoPar in lstGenericsModelParMap)
                        {
                            GenericParams mp = new GenericParams();
                            mp.ParameterID = Int32.Parse(genInfoPar.Parameter_id);
                            mp.ParameterName = genInfoPar.ParameterName;
                            mp.ParameterDisplayValue = genInfoPar.ParameterDisplayValue;
                            if (!string.IsNullOrWhiteSpace(genInfoPar.Symbol))
                                mp.ParameterSymbol = genInfoPar.Symbol;
                            if (!string.IsNullOrWhiteSpace(genInfoPar.Label) && !(genInfoPar.Label).Equals("n/a"))
                                mp.ParameterLabel = genInfoPar.Label;
                            mp.ParameterType = genInfoPar.ParameterType;
                            mp.ParameterValue = genInfoPar.Value;

                            mpList.Add(mp);
                            mp = null;
                        }
                        genInfo.GenericParameters.AddRange(mpList);
                        mpList = null;
                    }

                    if (test12 != null && test12.Count > 0)
                    {
                        List<GenericParams> gpList = new List<GenericParams>();
                        float test13 = (from info in test12
                                        select float.Parse(info.Value, CultureInfo.InvariantCulture.NumberFormat)).Min();
                        if (test13 > 0.0)
                        {
                            GenericParams gp = new GenericParams();
                            gp.ParameterID = -1;
                            gp.ParameterName = "US Price 1000-4999";
                            gp.ParameterValue = test13.ToString();
                            gp.ParameterDisplayValue = "$" + test13.ToString();

                            gpList.Add(gp);
                            genInfo.GenericParameters.AddRange(gpList);
                            gpList = null;
                            gp = null;
                        }
                    }
                    genericInfo.Add(genInfo);
                    genInfo = null;
                    lstGenericsModelParMap = null;
                    lstGeneric = null;
                }
                //}
            });

            return genericInfo;
        }

        /// <summary>
        /// gets display value for the parameter
        /// </summary>
        /// <param name="mp"></param>
        /// <returns></returns>
        public static string GetDisplayValue(string m1, string m2, string m3)
        {
            string displayValue = string.Empty;
            string finalSym = string.Empty;
            string finalLabel = string.Empty;

            if (!string.IsNullOrWhiteSpace(m2) && !m2.Equals("n/a"))
                finalLabel = m3;

            if (!string.IsNullOrWhiteSpace(m2))
                finalSym = m2;

            if (!string.IsNullOrWhiteSpace(finalSym))
                displayValue = Convert.ToDecimal(m1).ToString() + finalSym;
            else if (!string.IsNullOrWhiteSpace(finalLabel))
                displayValue = Convert.ToDecimal(m1).ToString() + finalLabel;//finalLabel

            return displayValue;
        }

        /// <summary>
        /// Get PCNPDN For Models
        /// </summary>
        /// <param name="lstModels">List of models for which PCDPDN is needed</param>
        /// <returns>List of ModelPCNPDNMap objects</returns>
        public static IEnumerable<ModelPCNPDNMap> GetPCNPDNForModelsMap(List<string> lstModels)
        {
            //Remove null/empty string from the list
            lstModels.RemoveAll(a => string.IsNullOrEmpty(a));

            //if tracking is enabled then log the response time
            StopWatch watch = new StopWatch();
            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
                watch.Start();

            List<ModelPCNPDNMap> lstModelPCNPDN = new List<ModelPCNPDNMap>();

            string modelXML = string.Empty;

            modelXML = GenerateXMLString(lstModels, "l", "m", "id");

            IEnumerable<CTPCNPDNContent> lstPCNPDNContent = GetModelPCNPDNFromDB(modelXML);

            List<RemovedProductMapping> lstModelPCNPDNRemoved = GetModelPCNPDNRemoved(lstModels);

            foreach (string model in lstModels)
            {
                try
                {
                    List<ModelPCNPDNContent> PCNPDNBundle = new List<ModelPCNPDNContent>();
                    ModelPCNPDNMap objModelPCNPDNMap = new ModelPCNPDNMap();

                    PCNPDNBundle = GetPCNPDNObjectForModel(model, lstPCNPDNContent, lstModelPCNPDNRemoved);

                    objModelPCNPDNMap.ModelNumber = model.ToUpper();
                    objModelPCNPDNMap.PCNPDNContent = PCNPDNBundle;

                    lstModelPCNPDN.Add(objModelPCNPDNMap);
                }
                catch (Exception ex)
                {
                    LogMessage("Error while building ModelPCNPDNMap object for model # -" + model + ". ", TraceEventType.Error, ex);
                }
            }
            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
            {
                watch.Stop();
                LogMessage("GetPCNPDN Method Response time for '" + String.Join(",", lstModels.Select(x => x.ToString()).ToArray()) + "' : " + watch.GetElapsedTimeSecs().ToString() + " second(s).", TraceEventType.Information);
            }
            return lstModelPCNPDN;
        }

        /// <summary>
        /// Returns a list of GenericModelDistiMap objects
        /// This method work with alias, example 3B01 which is a alias and 3B01G is generic
        /// </summary>
        /// <param name="lstGenerics">List of generics</param>
        /// <param name="countryCode">Country code</param>
        public static GenericModelDistiByCountry GetPPABundle(List<string> lstGenerics, string countryCode, List<string> lstParams)
        {
            lstParams = ValidatParameterList(lstParams);
            //Remove null/empty string from the list
            lstGenerics.RemoveAll(a => string.IsNullOrEmpty(a));

            //if tracking is enabled then log the response time
            StopWatch watch = new StopWatch();
            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
                watch.Start();

            GenericModelDistiByCountry genericModelDisti = new GenericModelDistiByCountry();
            try
            {
                string paramsXML = string.Empty;
                genericModelDisti.Country = countryCode;
                genericModelDisti.Distributors = GetDistributorInfoForCountry(countryCode);

                List<GenericModelDistiMap> genericModelDistiMaps = new List<GenericModelDistiMap>();

                //Generate generic xml
                string genericsXML = GenerateXMLString(lstGenerics, "l", "g", "id");

                //Generate XML of parameters to pass as DB parameter
                if (lstParams.Count != 0)
                {
                    if (!lstParams.Contains(getConfigValue("Param_DeliveryUnit")))
                    {
                        lstParams.Add(getConfigValue("Param_DeliveryUnit"));
                    }
                    paramsXML = GenerateXMLString(lstParams, "l", "p", "id");
                }
                else
                {
                    paramsXML = "";
                }

                string genericInfoParametersXML = GenerateXMLString(GetGenricInfoParameter(), "l", "p", "id");

                IEnumerable<CTGenericInformation> lstGenericsInfo = GetGenericInfo(genericsXML, genericInfoParametersXML);


                List<string> lstTempGeneric = new List<string>();
                lstTempGeneric = lstGenerics.Select(p => p).ToList();
                foreach (string generic in lstGenerics)
                {
                    var temp = lstGenericsInfo.Where(p => string.Equals(p.GenericNumber, generic, StringComparison.OrdinalIgnoreCase) || string.Equals(p.Alias, generic, StringComparison.OrdinalIgnoreCase)).Select(p => p);
                    if (temp.Count() > 0)
                    {
                        if (!string.IsNullOrEmpty(temp.FirstOrDefault().Alias))
                        {
                            lstTempGeneric.Remove(generic);
                            lstTempGeneric.Add(temp.FirstOrDefault().GenericNumber);
                        }
                    }
                }

                genericsXML = GenerateXMLString(lstTempGeneric, "l", "g", "id");

                IEnumerable<ModelParMap> enumGenericsModelsInfo = GetGenericModelsInfo(genericsXML, paramsXML);

                foreach (string generic in lstGenerics)
                {
                    genericModelDistiMaps.Add(GetModelDisti(generic, countryCode, enumGenericsModelsInfo, lstGenericsInfo));
                }
                genericModelDisti.Generics = genericModelDistiMaps;
            }
            catch (Exception ex)
            {
                LogMessage("There is issue while building PPABundle object for generic.", TraceEventType.Error, ex);
            }
            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
            {
                watch.Stop();
                LogMessage("GetPPA Method Response time for '" + String.Join(",", lstGenerics.Select(x => x.ToString()).ToArray()) + "' : " + watch.GetElapsedTimeSecs().ToString() + " second(s)", TraceEventType.Information);
            }
            return genericModelDisti;
        }

        /// <summary>
        /// Get generic information for given models
        /// This method work with alias, example 3B01 which is a alias and 3B01G is generic
        /// </summary>
        /// <param name="lstModels">List of models</param>
        /// <returns></returns>
        public static List<ModelGeneric> GetModelsGeneric(List<string> lstModels)
        {
            List<ModelGeneric> response = new List<ModelGeneric>();

            //Remove null/empty string from the list
            lstModels.RemoveAll(a => string.IsNullOrEmpty(a));
            //Sort the list
            lstModels.Sort();

            //if tracking is enabled then log the response time
            StopWatch watch = new StopWatch();
            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
                watch.Start();

            //Generate XML of models to pass as DB parameter
            string modelXML = string.Empty;
            modelXML = GenerateXMLString(lstModels, "l", "m", "id");

            IEnumerable<CTGenericInfoByModel> enumGenericInfoByModel = null;
            using (var pdbEntities = new PDBEntities())
            {
                enumGenericInfoByModel = (from m in pdbEntities.GetGenericInfoByModelXML(modelXML)
                                          select m).ToList();
            }

            foreach (string model in lstModels)
            {
                try
                {
                    var temp = enumGenericInfoByModel.Where(p => string.Equals(p.ModelNumber, model, StringComparison.OrdinalIgnoreCase));
                    if (temp.Count() > 0)
                    {
                        ModelGeneric objModelGeneric = new ModelGeneric();
                        objModelGeneric.GenericNumber = string.IsNullOrEmpty(temp.FirstOrDefault().Alias) ? temp.FirstOrDefault().GenericNumber : temp.FirstOrDefault().Alias;
                        objModelGeneric.ModelNumber = temp.FirstOrDefault().ModelNumber;
                        response.Add(objModelGeneric);
                    }
                }
                catch
                {
                    LogMessage("Error while gettting generic for 'ad8030'", TraceEventType.Error);
                }
            }
            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
            {
                watch.Stop();
                LogMessage("GetModelsGeneric Method Response time for '" + String.Join(",", lstModels.Select(x => x.ToString()).ToArray()) + "' : " + watch.GetElapsedTimeSecs().ToString() + " second(s).", TraceEventType.Information);
            }
            return response;
        }
        #endregion

        #region private method
        /// <summary>
        /// Gets a genericmodelmap object
        /// </summary>
        /// <param name="genericOrEvals"></param>
        /// <returns></returns>

        private static GenericModelMap GetModelsByGenericsOrEvals(string genericOrEvals, List<string> lstParams, List<ModelParMap> lstGenericsOrEvalsModelParMap, IEnumerable<Adisp_GetLTCSymbolInformation_Result> lstLTCSymbolInformation, List<CTGenericInformation> lstGenericInfo = null, bool includePCNPDN = false)
        {

            List<Models> models = new List<Models>();
            GenericModelMap genericModelMap = new GenericModelMap();
            try
            {
                //Get distinct modles from lstGenericsModelParMap
                var modelquery = lstGenericsOrEvalsModelParMap.Select(p => p.ModelNumber).Distinct();

                string modelXML = string.Empty;

                modelXML = GenerateXMLString(modelquery.ToList(), "l", "m", "id");

                //Get the model generic information
                IEnumerable<CTGenericInfoByModel> enumGenericInfoByModel = null;
                using (var pdbEntities = new PDBEntities())
                {
                    enumGenericInfoByModel = (from m in pdbEntities.GetGenericInfoByModelXML(modelXML)
                                              select m).ToList();
                }



                //If includePCNPDN is true then 
                IEnumerable<CTPCNPDNContent> enumPCNPDNContent = null;
                List<RemovedProductMapping> lstModelPCNPDNRemoved = new List<RemovedProductMapping>();
                if (includePCNPDN)
                {
                    enumPCNPDNContent = GetModelPCNPDNFromDB(modelXML);
                    lstModelPCNPDNRemoved = GetModelPCNPDNRemoved(modelquery.ToList());
                }

                //Get model symbols details from DB for generic
                IEnumerable<CTModelSymbols> enumModelSymbols = GetModelSymbolsByGeneric(genericOrEvals);

                //Get the model information (like description) from EB_EvalBoardComponent table
                IEnumerable<CTEvalBoardModelInfo> enumEvalBoardModelsInfo = GetEvalBoardModelsInfo(genericOrEvals);

                foreach (string model in modelquery)
                {
                    #region Process each model one by one
                    try
                    {
                        models.Add(GetModelObject(model, lstParams, lstModelPCNPDNRemoved, lstGenericsOrEvalsModelParMap, enumPCNPDNContent, enumModelSymbols, enumGenericInfoByModel, enumEvalBoardModelsInfo));
                    }
                    catch (Exception ex)
                    {
                        LogMessage("Error while building Models object for model # - " + model + ". ", TraceEventType.Error, ex);
                    }
                    #endregion
                }
                //Get the LTC part symbol information

                //lstGenericInfo will be null for Eval baords, its only applicable to Generic
                if (lstGenericInfo != null && lstGenericInfo.Count() > 0)
                {
                    GenericInfo objGenericInfo = GetGenericInfoObject(lstGenericInfo);
                    //Populate the GenericModelMap object
                    genericModelMap.Generic = objGenericInfo.Generic;
                    genericModelMap.PricePointParameterID = objGenericInfo.PricePointParameterID;
                    //For evals lstGenericInfo will always be null so no need of setting up rest of the generic information, it will be exposed as null
                    genericModelMap.Status = objGenericInfo.Status;
                    genericModelMap.VintageDate = objGenericInfo.VintageDate;
                    genericModelMap.ObsoleteAvailability = objGenericInfo.ObsoleteAvailability;
                    genericModelMap.ReplacementCompatibility = objGenericInfo.ReplacementCompatibility;
                    genericModelMap.ReplacementPart = objGenericInfo.ReplacementPart;
                    genericModelMap.MfgPlanGroupCode = objGenericInfo.MfgPlanGroupCode;
                    var tempSymbolInfo = (lstLTCSymbolInformation != null && lstLTCSymbolInformation.Any()) ? lstLTCSymbolInformation.Where(p => p.GenericNumber.Equals(genericModelMap.Generic, StringComparison.InvariantCultureIgnoreCase)
                                                                            || !string.IsNullOrWhiteSpace(p.Alias) && Convert.ToString(p.Alias).Equals(genericModelMap.Generic, StringComparison.InvariantCultureIgnoreCase)).Select(p => p) : null;
                    if (tempSymbolInfo != null && tempSymbolInfo.Any())
                    {
                        genericModelMap.Symbols = new ModelSymbols()
                        {
                            BXLFile = tempSymbolInfo.FirstOrDefault().File_Name,
                            url = string.Format(getConfigValue("SymbolUrl"), tempSymbolInfo.FirstOrDefault().File_Name)
                        };
                    }
                }
                else
                {
                    genericModelMap.Generic = genericOrEvals.ToUpper();
                }
                genericModelMap.Models = models;
            }
            catch (Exception ex)
            {
                LogMessage("Error in (private) GetModels  methode for generic # -" + genericOrEvals + ". ", TraceEventType.Error, ex);
            }
            return (genericModelMap);
        }

        /// <summary>
        /// GetGenericInfo Object
        /// </summary>
        /// <param name="lstGenericInfo">lstGenericInfo</param>
        /// <returns></returns>
        private static GenericInfo GetGenericInfoObject(List<CTGenericInformation> lstGenericInfo)
        {

            GenericInfo objGenericInfo = new GenericInfo();

            objGenericInfo.Generic = string.IsNullOrEmpty(lstGenericInfo.First().Alias) ? lstGenericInfo.First().GenericNumber : lstGenericInfo.First().Alias;

            if (lstGenericInfo.First().PricePoint != null && lstGenericInfo.First().PricePoint > 0)
                objGenericInfo.PricePointParameterID = Convert.ToInt32(lstGenericInfo.First().PricePoint);

            var temp = lstGenericInfo.Where(p => p.parameteR_id.ToString() == getConfigValue("Param_JEDECLifeCycleDesc")).Select(p => p);

            if (temp.Count() > 0)
            {
                string pseudoStatus = Convert.ToString(lstGenericInfo.Where(p => p.parameteR_id.ToString() == getConfigValue("Param_JEDECLifeCycleDesc")).Select(p => p).First().PseudoStatus);
                if (!string.IsNullOrEmpty(pseudoStatus))
                {
                    objGenericInfo.Status = pseudoStatus;
                }
                else
                {
                    objGenericInfo.Status = GetGenericDisplayStatus(lstGenericInfo.Where(p => p.parameteR_id.ToString() == getConfigValue("Param_JEDECLifeCycleDesc")).Select(p => p).First().value.ToString());
                }

                objGenericInfo.VintageDate = lstGenericInfo.Where(p => p.parameteR_id.ToString() == getConfigValue("Param_VintageDate")).Select(p => p).First().value.ToString();
                if ((lstGenericInfo.Where(p => p.parameteR_id.ToString() == getConfigValue("Param_ObsoleteAvailability")).Select(p => p).Count() > 0))
                    objGenericInfo.ObsoleteAvailability = lstGenericInfo.Where(p => p.parameteR_id.ToString() == getConfigValue("Param_ObsoleteAvailability")).Select(p => p).First().value.ToString();
                if ((lstGenericInfo.Where(p => p.parameteR_id.ToString() == getConfigValue("Param_ReplacementCompatibility")).Select(p => p).Count() > 0))
                    objGenericInfo.ReplacementCompatibility = lstGenericInfo.Where(p => p.parameteR_id.ToString() == getConfigValue("Param_ReplacementCompatibility")).Select(p => p).First().value.ToString();
                if ((lstGenericInfo.Where(p => p.parameteR_id.ToString() == getConfigValue("Param_ReplacementPart")).Select(p => p).Count() > 0))
                    objGenericInfo.ReplacementPart = lstGenericInfo.Where(p => p.parameteR_id.ToString() == getConfigValue("Param_ReplacementPart")).Select(p => p).First().value.ToString();
                objGenericInfo.MfgPlanGroupCode = lstGenericInfo.Select(p => p.mfgplangroupcode).FirstOrDefault();

            }
            return objGenericInfo;
        }

        private static GenericParameters GetGenParamInfoResult(List<Adisp_GetGenericParameterInformation_Result> lstGenParamInfo)
        {
            GenericParameters genParams = new GenericParameters();
            genParams.Parameters = new List<ModelParams>();
            string[] extraParams = new string[] { "Status", "US Price 1000-4999", "Product Description" };

            if (lstGenParamInfo != null && lstGenParamInfo.Count() > 0)
            {
                foreach (var param in lstGenParamInfo)
                {
                    ModelParams paramInfo = new ModelParams();
                    paramInfo.ParameterID = param.ParameterID;
                    paramInfo.ParameterName = param.Name;

                    genParams.Parameters.Add(paramInfo);
                    paramInfo = null;
                }

                foreach (string param in extraParams)
                {
                    ModelParams staticList = new ModelParams();
                    if (param.Equals("Status"))
                        staticList.ParameterID = 0;
                    else if (param.Equals("US Price 1000-4999"))
                        staticList.ParameterID = -1;
                    else if (param.Equals("Product Description"))
                        staticList.ParameterID = -2;
                    staticList.ParameterName = param;
                    genParams.Parameters.Add(staticList);
                    staticList = null;
                }
            }


            return genParams;
        }


        /// <summary>
        /// Gets a modelprice object
        /// </summary>
        /// <param name="modelNumber"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        private static double GetPrice(string modelNumber, string quantity)
        {
            double price = 0;
            try
            {
                using (var cdbEntities = new CDBEntities())
                {
                    //Gets the list of all model parameters for a particular generic from PDB
                    var modelpricequery = (from map in cdbEntities.getModelPrice("NAM1", "01", "USD", modelNumber, quantity, "")
                                           select map).ToList();

                    if (modelpricequery.Count > 0)
                    {
                        price = Convert.ToDouble(modelpricequery.First().usd_list_price) / 100;
                    }
                }

            }
            catch (Exception ex)
            {
                LogMessage("Error in GetPrice methode for model # - " + modelNumber + ". ", TraceEventType.Error, ex);
            }
            return (price);
        }

        /// <summary>
        /// Gets the distributor information for a country
        /// </summary>
        /// <param name="ISOCountryCode">CountryCode</param>
        /// <returns></returns>
        private static List<DistiInfo> GetDistributorInfoForCountry(string ISOCountryCode)
        {
            List<DistiInfo> lstDistributors = new List<DistiInfo>();
            try
            {
                using (var pdbEntities = new PDBEntities())
                {
                    lstDistributors = (from distiInfo in pdbEntities.GetDistiInfoByCountry(ISOCountryCode)
                                       select new DistiInfo
                                       {
                                           DistiID = distiInfo.disti_id,
                                           DistiHomeURL = distiInfo.homeUrl,
                                           DistiName = distiInfo.displayName
                                       })
                                       .ToList();
                }
            }
            catch (Exception ex)
            {
                LogMessage("Error in GetDistributorInfoForCountry methode for Country code - " + ISOCountryCode + ". ", TraceEventType.Error, ex);
            }
            return lstDistributors;
        }

        /// <summary>
        ///  Returns a GenericModelDistiMap object
        /// </summary>
        /// <param name="generic">Generic</param>
        /// <param name="countryCode">Country code</param>
        /// <returns></returns>
        private static GenericModelDistiMap GetModelDisti(string generic, string countryCode, IEnumerable<ModelParMap> enumGenericsModelsInfo, IEnumerable<CTGenericInformation> enumGenericsInfo = null)
        {
            IEnumerable<CTDistiModelStockInfo> enumDistiModelStockInfo = null;
            List<ModelDistiInfo> models = new List<ModelDistiInfo>();
            IList<DistiInfo> lstDistiInfo = new List<DistiInfo>();
            GenericModelDistiMap genericModelDistiMap = new GenericModelDistiMap();

            try
            {

                using (var pdbEntities = new PDBEntities())
                {


                    //Get all models, iterate to form Model object with a list of Parameters
                    var modelquery = enumGenericsModelsInfo.Select(p => p.ModelNumber).Distinct();

                    //Generate XML of models to pass as DB call parameter
                    string modelXML = string.Empty;
                    modelXML = GenerateXMLString(modelquery, "l", "m", "id");

                    //Get PCN/PDN for models
                    IEnumerable<CTPCNPDNContent> enumPCNPDNContent = GetModelPCNPDNFromDB(modelXML);

                    enumDistiModelStockInfo = (from info in pdbEntities.GetDistiStockAvailabilityOfModelXML(modelXML, countryCode)
                                               select info).ToList();


                    List<RemovedProductMapping> lstModelPCNPDNRemoved = GetModelPCNPDNRemoved(modelquery.ToList());

                    //Get ADI inventory for Models
                    IEnumerable<CTModelAdiInventory> enumModelsAdiInventory = GetModelAdiInventory(GenerateXMLString(modelquery, "l", "p", "id"), countryCode);

                    #region Form the Models node
                    foreach (string model in modelquery)
                    {
                        try
                        {
                            models.Add(new ModelDistiInfo()
                            {
                                ModelNumber = model,
                                ModelParameters = enumGenericsModelsInfo.Where(p => p.ModelNumber == model)
                                                    .Select(p => new ModelParams()
                                                    {
                                                        ParameterID = p.Parameter_Id,
                                                        ParameterName = p.Parameter_Name,
                                                        ParameterValue = p.Value
                                                    }).ToList(),
                                USListPrice = GetPrice(model, enumGenericsModelsInfo
                                .Where(p => p.ModelNumber == model)
                                .Where(p => p.Parameter_Name == "DeliveryUnit")
                                .First().Value),
                                ModelDistributors = GetDistributorInfoForModel(model, enumDistiModelStockInfo),
                                PCNPDNContent = GetPCNPDNObjectForModel(model, enumPCNPDNContent, lstModelPCNPDNRemoved),
                                ModelInventory = GetModelAdiInventoryFromList(model, enumModelsAdiInventory)
                            });
                        }
                        catch (Exception ex)
                        {
                            LogMessage("Error while building ModelDistiInfo object for " + model.ToUpper() + ". ", TraceEventType.Error, ex);
                        }
                    }
                    #endregion

                    //Get the current processing generic information from list of all generics info(lstGenericsInfo)
                    List<CTGenericInformation> lstGenericInfo = FilterGenericInfoForGeneric(generic, enumGenericsInfo);

                    GenericInfo objGenericInfo = GetGenericInfoObject(lstGenericInfo);
                    //Populate the GenericModelMap object
                    genericModelDistiMap.Generic = objGenericInfo.Generic;
                    genericModelDistiMap.Status = objGenericInfo.Status;
                    genericModelDistiMap.VintageDate = objGenericInfo.VintageDate;
                    genericModelDistiMap.ObsoleteAvailability = objGenericInfo.ObsoleteAvailability;
                    genericModelDistiMap.ReplacementCompatibility = objGenericInfo.ReplacementCompatibility;
                    genericModelDistiMap.ReplacementPart = objGenericInfo.ReplacementPart;
                    genericModelDistiMap.Models = models;
                    genericModelDistiMap.MfgPlanGroupCode = objGenericInfo.MfgPlanGroupCode;
                }

            }
            catch (Exception ex)
            {
                LogMessage("Error in GetModelDisti methode for generic # :" + generic.ToUpper() + ". ", TraceEventType.Error, ex);
            }
            return (genericModelDistiMap);
        }

        /// <summary>
        /// Filter/Get the Model's Adi Quantity from enumModelsAdiInventory
        /// </summary>
        /// <param name="model"></param>
        /// <param name="enumModelsAdiInventory"></param>
        /// <returns></returns>
        private static string GetModelAdiInventoryFromList(string model, IEnumerable<CTModelAdiInventory> enumModelsAdiInventory)
        {
            string qunatity = "";
            var modelAdiInventory = enumModelsAdiInventory.Where(p => p.modelnumber.ToLower().Trim() == model.ToLower().Trim()).Select(p => p).ToList();
            if (modelAdiInventory.Count() > 0)
            {
                qunatity = modelAdiInventory.FirstOrDefault().quantity.ToString();
            }
            return qunatity;
        }

        /// <summary>
        /// Returns a dictionary with integer key and ModelDistiQuantity value
        /// </summary>
        /// <param name="distinctModel">ModelNumber</param>
        /// <param name="lstDistiModelStock">List of CTDistiModelStockInfo</param>
        /// <returns></returns>
        private static Dictionary<int, ModelDistiQuantity> GetDistributorInfoForModel(string distinctModel, IEnumerable<CTDistiModelStockInfo> lstDistiModelStock)
        {
            //Foreach model get the quantity, part url, disti info
            DistiModel objDistiModel = null;
            Dictionary<int, ModelDistiQuantity> dictModelDistiQuantity = null;

            try
            {

                if (!string.IsNullOrEmpty(distinctModel))
                {
                    objDistiModel = new DistiModel();
                    //Get the model quantity info from lstDistiModelStockInfo list
                    var modelsQuantityInfo = from qtyInfo in lstDistiModelStock
                                             where qtyInfo.modelNumber == distinctModel
                                             select qtyInfo;


                    ModelDistiQuantity objModelDistiQuantity = null;
                    dictModelDistiQuantity = new Dictionary<int, ModelDistiQuantity>();
                    //For each distributor under the modelsQuantityInfo build ModelDistiQuantity object
                    foreach (var modelQuantity in modelsQuantityInfo)
                    {
                        objModelDistiQuantity = new ModelDistiQuantity();
                        objModelDistiQuantity.Quantity = Convert.ToInt32(modelQuantity.quantity);
                        objModelDistiQuantity.PartURL = modelQuantity.url.ToString();
                        dictModelDistiQuantity.Add(modelQuantity.Disti_id, objModelDistiQuantity);
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessage("Error in GetDistributorInfoForModel methode for model id - " + distinctModel + ". ", TraceEventType.Error, ex);
            }
            return dictModelDistiQuantity;
        }

        #region common methods

        /// <summary>
        /// Returns a Models object
        /// </summary>
        /// <param name="model">Model Number</param>
        /// <param name="enumModelparquery">Models parameter details</param>
        /// <param name="enumPCNPDNContent">Models PCN/PDN details</param>
        /// <param name="enumModelSymbols">Models parameter details</param>
        /// <returns>Returns Models object</returns>
        private static Models GetModelObject(string model, List<string> lstParams, List<RemovedProductMapping> lstModelPCNPDNRemoved, IEnumerable<ModelParMap> enumModelparquery,
            IEnumerable<CTPCNPDNContent> enumPCNPDNContent, IEnumerable<CTModelSymbols> enumModelSymbols, IEnumerable<CTGenericInfoByModel> enumGenericInfoByModel,
            IEnumerable<CTEvalBoardModelInfo> enumEvalBoardModelsInfo = null, IEnumerable<Adisp_GetLTCSymbolInformation_Result> enumLTCSymbolInformation = null, IEnumerable<CTGenericInformation> enumGenericsInfo = null)
        {

            Models objModels = null;
            string modelDescription = null;
            string packing = null;
            #region model desc
            if (enumEvalBoardModelsInfo != null)
            {
                var filter = enumEvalBoardModelsInfo.Where(p => p.OrderableModelNumber.ToLower().Trim() == model.ToLower().Trim()).Select(p => p);
                if (filter.Count() > 0)
                {
                    modelDescription = filter.First().DescriptionEN;
                }
            }
            using (var pdbEntities = new PDBEntities())
            {
                var dispval = (from info in pdbEntities.ADIsp_displayparametervaluebymodel(model)
                               select info).ToList();
                var dispvalbymodel = dispval.Select(p => p.packing_option_uom);
                packing = dispvalbymodel.FirstOrDefault();

            }
            #endregion



            try
            {
                if (lstParams == null || lstParams.Count == 0)
                {

                    objModels = new Models();
                    objModels.ModelNumber = model;
                    objModels.ModelParameters = enumModelparquery.Where(p => p.ModelNumber == model)
                                        .Select(p => new ModelParams()
                                        {
                                            ParameterID = p.Parameter_Id,
                                            ParameterName = p.Parameter_Name,
                                            ParameterValue = p.Value
                                        }).ToList();


                    var temp = enumModelparquery.Where(p => p.ModelNumber == model).Where(p => p.Parameter_Name == "DeliveryUnit");
                    if (temp.Count() > 0)
                        objModels.USListPrice = GetPrice(model, temp.First().Value);
                    else
                    {
                        objModels.USListPrice = null;
                    }
                    objModels.PAcking_Option_Uom = packing;
                    objModels.Description = modelDescription;
                    objModels.PCNPDNContent = GetPCNPDNObjectForModel(model, enumPCNPDNContent, lstModelPCNPDNRemoved);
                    objModels.LastUpdatedDate = enumModelparquery.Where(p => p.ModelNumber.ToUpper() == model.ToUpper())
                                        .Select(p => p.modification_date).Max().ToString();
                    objModels.Generic = enumGenericInfoByModel.Where(p => p.ModelNumber.ToUpper() == model.ToUpper()).Select(p => p).FirstOrDefault().GenericNumber;
                    var tempSymbolInfo = (enumLTCSymbolInformation != null && enumLTCSymbolInformation.Any()) ? enumLTCSymbolInformation.Where(p => p.GenericNumber.Equals(objModels.Generic, StringComparison.InvariantCultureIgnoreCase)
                                                                                || !string.IsNullOrWhiteSpace(p.Alias) && Convert.ToString(p.Alias).Equals(objModels.Generic, StringComparison.InvariantCultureIgnoreCase)).Select(p => p) : null;
                    if (tempSymbolInfo != null && tempSymbolInfo.Any())
                    {
                        objModels.Symbols = new ModelSymbols()
                        {
                            BXLFile = enumLTCSymbolInformation.FirstOrDefault().File_Name,
                            url = string.Format(getConfigValue("SymbolUrl"), enumLTCSymbolInformation.FirstOrDefault().File_Name)
                        };
                    }
                    else
                    {
                        objModels.Symbols = GetModelSymbolsForModel(model, enumModelSymbols);
                    }
                }
                else
                {

                    objModels = new Models();
                    objModels.ModelNumber = model;
                    objModels.ModelParameters = enumModelparquery.Where(p => p.ModelNumber == model && lstParams.Contains(p.Parameter_Id.ToString()))
                                        .Select(p => new ModelParams()
                                        {
                                            ParameterID = p.Parameter_Id,
                                            ParameterName = p.Parameter_Name,
                                            ParameterValue = p.Value
                                        }).ToList();



                    var temp = enumModelparquery.Where(p => p.ModelNumber == model).Where(p => p.Parameter_Name == "DeliveryUnit");
                    if (temp.Count() > 0)
                        objModels.USListPrice = GetPrice(model, temp.First().Value);
                    else
                        objModels.USListPrice = null;
                    objModels.PAcking_Option_Uom = packing;
                    objModels.Description = modelDescription;
                    objModels.PCNPDNContent = GetPCNPDNObjectForModel(model, enumPCNPDNContent, lstModelPCNPDNRemoved);
                    objModels.LastUpdatedDate = enumModelparquery.Where(p => p.ModelNumber.ToUpper() == model.ToUpper())
                                        .Select(p => p.modification_date).Max().ToString();
                    objModels.Generic = enumGenericInfoByModel.Where(p => p.ModelNumber.ToUpper() == model.ToUpper()).Select(p => p).FirstOrDefault().GenericNumber;
                    var tempSymbolInfo = (enumLTCSymbolInformation != null && enumLTCSymbolInformation.Any()) ? enumLTCSymbolInformation.Where(p => p.GenericNumber.Equals(objModels.Generic, StringComparison.InvariantCultureIgnoreCase)
                                                                            || !string.IsNullOrWhiteSpace(p.Alias) && Convert.ToString(p.Alias).Equals(objModels.Generic, StringComparison.InvariantCultureIgnoreCase)).Select(p => p) : null;
                    if (tempSymbolInfo != null && tempSymbolInfo.Any())
                    {
                        objModels.Symbols = new ModelSymbols()
                        {
                            BXLFile = enumLTCSymbolInformation.FirstOrDefault().File_Name,
                            url = string.Format(getConfigValue("SymbolUrl"), enumLTCSymbolInformation.FirstOrDefault().File_Name)
                        };
                    }
                    else
                    {
                        objModels.Symbols = GetModelSymbolsForModel(model, enumModelSymbols);
                    }
                }
            }

            catch (Exception ex)
            {
                LogMessage("Error in GetModelObject methode for Model id - " + model.ToUpper() + ".", TraceEventType.Error, ex);
            }
            return objModels;
        }

        /// <summary>
        /// Retrieves the symbol information for given model from list 
        /// </summary>
        /// <param name="model">Model number</param>
        /// <param name="enumModelSymbols">List of CTModelSymbols which has all the symbols information for all the model belongs to a generic</param>
        /// <returns>returns ModelSymbols</returns>
        private static ModelSymbols GetModelSymbolsForModel(string model, IEnumerable<CTModelSymbols> enumModelSymbols)
        {
            ModelSymbols objModelSymbols = null;
            try
            {
                var modelSymbol = enumModelSymbols.Where(p => p.ModelNumber.Equals(model)).Select(p => p);
                if (modelSymbol.Count() > 0)
                {
                    objModelSymbols = new ModelSymbols()
                    {
                        url = string.Format(getConfigValue("SymbolUrl"), modelSymbol.First().BXLFile),
                        BXLFile = modelSymbol.First().BXLFile
                    };
                }
            }
            catch (Exception ex)
            {
                LogMessage("Error in GetModelSymbolsForModel methode for Model id - " + model.ToUpper() + ".", TraceEventType.Error, ex);
            }
            return objModelSymbols;
        }

        /// <summary>
        /// Get the model information (like description) from EB_EvalBoardComponent table
        /// </summary>
        /// <param name="genericNumber">Generic number</param>
        /// <returns>List of CTModelSymbols</returns>
        private static IEnumerable<CTEvalBoardModelInfo> GetEvalBoardModelsInfo(string ebName)
        {
            using (var pdbEntities = new PDBEntities())
            {
                return (from item in pdbEntities.GetEvalBoardModelsInfo(ebName)
                        select item).ToList();
            }
        }


        /// <summary>
        /// Get Model Symbols By Generic from database
        /// </summary>
        /// <param name="genericNumber">Generic number</param>
        /// <returns>List of CTModelSymbols</returns>
        private static IEnumerable<CTModelSymbols> GetModelSymbolsByGeneric(string genericNumber)
        {
            using (var pdbEntities = new PDBEntities())
            {
                return (from symbols in pdbEntities.GetSymbolsByGeneric(genericNumber)
                        select symbols).ToList();
            }
        }

        /// <summary>
        /// Filters Generic Info Object from list of generics info 
        /// </summary>
        /// <param name="generic">Generic for which info is needed</param>
        /// <param name="lstGenericsInfo">List of All generics info</param>
        /// <returns>returns CTGenericInformation object</returns>
        private static List<CTGenericInformation> FilterGenericInfoForGeneric(string generic, IEnumerable<CTGenericInformation> lstGenericsInfo)
        {
            //var objGenericInfo = (from genricInfo in lstGenericsInfo
            //                      where genricInfo.GenericNumber.ToLower().Trim().Equals(generic.ToLower().Trim())
            //                      select genricInfo).ToList();

            //var objGenericInfo = lstGenericsInfo.Where(p => string.Equals(p.GenericNumber, generic, StringComparison.OrdinalIgnoreCase) || string.Equals(p.Alias, generic, StringComparison.OrdinalIgnoreCase)).Select(p => p).ToList();
            var objGenericInfo = lstGenericsInfo.Where(p => string.Equals(p.GenericNumber, generic, StringComparison.OrdinalIgnoreCase) || string.Equals(p.Name, generic, StringComparison.OrdinalIgnoreCase) || string.Equals(p.Alias, generic, StringComparison.OrdinalIgnoreCase)).Select(p => p).ToList();

            return objGenericInfo;
        }

        /// <summary>
        /// Gets generics info from database 
        /// </summary>
        /// <param name="genericsXML">Generic xml</param>
        /// <returns></returns>
        private static IEnumerable<CTGenericInformation> GetGenericInfo(string genericsXML, string genericInfoParametersXML)
        {
            IEnumerable<CTGenericInformation> lstGenericsInfo = null;
            using (var pdbEntities = new PDBEntities())
            {
                lstGenericsInfo = (from info in pdbEntities.GetGenericInformation(genericsXML, genericInfoParametersXML)
                                   select info).ToList();
            }
            return lstGenericsInfo;
        }

        /// <summary>
        /// Get Models And Buy Ids
        /// </summary>
        /// <param name="modelsNameXML">modelsName XML</param>
        /// <returns></returns>
        private static IEnumerable<ADISP_GetBuyIdByModels_Result> GetModelsAndBuyIds(string modelsNameXML)
        {
            IEnumerable<ADISP_GetBuyIdByModels_Result> lstModelsAndBuyIds = null;
            using (var pdbEntities = new PDBEntities())
            {
                lstModelsAndBuyIds = (from info in pdbEntities.ADISP_GetBuyIdByModels(modelsNameXML)
                                      select info).ToList();
            }
            return lstModelsAndBuyIds;
        }

        /// <summary>
        /// Get Symbol Info For LTC Legacy Parts
        /// </summary>
        /// <param name="genericsXML">Generic xml</param>
        /// <returns></returns>
        private static IEnumerable<Adisp_GetLTCSymbolInformation_Result> GetSymbolInfoForGenerics(string genericsXML)
        {
            IEnumerable<Adisp_GetLTCSymbolInformation_Result> lstGenericsInfo = null;
            using (var pdbEntities = new PDBEntities())
            {
                lstGenericsInfo = (from info in pdbEntities.Adisp_GetLTCSymbolInformation(genericsXML)
                                   select info).ToList();
            }
            return lstGenericsInfo;
        }
       
        private static IEnumerable<Adisp_GetGenericParameterListandValue1_Result> GetGenParamListValue(string genericsxml, string paramsxml)
        {
            IEnumerable<Adisp_GetGenericParameterListandValue1_Result> lstGenericsInfo = null;
            using (var pdbEntities = new PDBEntities())
            {
                lstGenericsInfo = (from info in pdbEntities.Adisp_GetGenericParameterListandValue1(genericsxml, paramsxml)
                                   select info).ToList();
            }
            return lstGenericsInfo;
        }
        private static List<Adisp_GetGenericParameterInformation_Result> GetGenParamInfo(string genericsXML)
        {
            List<Adisp_GetGenericParameterInformation_Result> GenericsParams = null;
            using (var pdbEntities = new PDBEntities())
            {
                GenericsParams = (from info in pdbEntities.Adisp_GetGenericParameterInformation(genericsXML)
                                  select info).ToList();
            }
            return GenericsParams;
        }
        /// <summary>
        /// Gets generics models info from database for passed generics as xml
        /// </summary>
        /// <param name="genericsXML">Generic xml</param>
        /// <returns></returns>
        private static IEnumerable<ModelParMap> GetGenericModelsInfo(string genericsXML, string paramsXML)
        {
            IEnumerable<ModelParMap> lstGenericsModelInfo = null;
            using (var pdbEntities = new PDBEntities())
            {
                lstGenericsModelInfo = (from info in pdbEntities.getGenericModelParMap(genericsXML, paramsXML)
                                        select info).ToList();
            }
            return lstGenericsModelInfo;
        }

        /// <summary>
        /// Gets the generic/parts/products models.
        /// </summary>
        /// <param name="genericsXML">The generics XML.</param>
        /// <returns></returns>
        private static IEnumerable<ADISP_GetGenEvalModels_Result> GetGenericModels(string genericsXML)
        {
            IEnumerable<ADISP_GetGenEvalModels_Result> lstGenericsModelInfo = null;
            using (var pdbEntities = new PDBEntities())
            {
                lstGenericsModelInfo = (from info in pdbEntities.ADISP_GetGenEvalModels(genericsXML)
                                        select info).ToList();
            }
            return lstGenericsModelInfo;
        }

        /// <summary>
        /// Gets Eval baords models info from database for passed Eval baords as xml
        /// </summary>
        /// <param name="genericsXML">Generic xml</param>
        /// <returns></returns>
        private static IEnumerable<ModelParMap> GetEvalModelsInfo(string evalXML, string paramXML)
        {
            IEnumerable<ModelParMap> lstEvalModelInfo = null;
            using (var pdbEntities = new PDBEntities())
            {
                lstEvalModelInfo = (from info in pdbEntities.GetEvalModelParMap(evalXML, paramXML)
                                    select info).ToList();
            }
            return lstEvalModelInfo;
        }

        /// <summary>
        /// Get PCN/PDN  Object For Model
        /// </summary>
        /// <param name="model">Model for which PCN/PDN object is needed</param>
        /// <param name="lstPCNPDNContent">List of all PCN/PDN content for set of models</param>
        /// <returns></returns>
        private static List<ModelPCNPDNContent> GetPCNPDNObjectForModel(string model, IEnumerable<CTPCNPDNContent> lstPCNPDNContent, List<RemovedProductMapping> lstModelPCNPDNRemoved)
        {
            List<ModelPCNPDNContent> PCNPDNBundle = new List<ModelPCNPDNContent>();
            try
            {
                if (lstPCNPDNContent != null)
                {
                    var filteredModelPCNPDN = from info in lstPCNPDNContent
                                              where info.modelNumber.ToLower().Trim().Equals(model.ToLower().Trim())
                                              select info;


                    foreach (var item in filteredModelPCNPDN)
                    {
                        ModelPCNPDNContent objModelPCNPDNContent = BuildPCNPDNObject(item, lstModelPCNPDNRemoved);
                        PCNPDNBundle.Add(objModelPCNPDNContent);
                    }
                }
            }
            catch (Exception ex)
            {
                LogMessage("Error in GetPCNPDNObjectForModel methode for Model id - " + model.ToUpper() + ".", TraceEventType.Error, ex);
            }
            return PCNPDNBundle;
        }

        /// <summary>
        /// Build one PCN/PDN Object 
        /// </summary>
        /// <param name="objPCNPDN"></param>
        /// <returns></returns>
        private static ModelPCNPDNContent BuildPCNPDNObject(CTPCNPDNContent objPCNPDN, List<RemovedProductMapping> lstModelPCNPDNRemoved)
        {
            bool isPCNPDNApplicable = true;
            var a = lstModelPCNPDNRemoved.Where(p => p.productChangeRevNumber == objPCNPDN.productChangeRevNumber && p.modelNumber == objPCNPDN.modelNumber && p.productChangeUnitID == objPCNPDN.productChangeUnitID)
                .Select(p => p);

            if (a != null)
            {
                if (a.Count() > 0)
                {
                    isPCNPDNApplicable = false;
                }
            }
            ModelPCNPDNContent objModelPCNPDNContent = new ModelPCNPDNContent();
            objModelPCNPDNContent.CreatedDate = Convert.ToString(objPCNPDN.createdDate);
            objModelPCNPDNContent.ModifiedDate = Convert.ToString(objPCNPDN.modifiedDate);
            objModelPCNPDNContent.ProductChangeUnitID = objPCNPDN.productChangeUnitID;
            objModelPCNPDNContent.ProductChangeType = objPCNPDN.productChangeType;
            objModelPCNPDNContent.Title = objPCNPDN.title;
            objModelPCNPDNContent.PublishDate = objPCNPDN.publishDate.ToString();
            objModelPCNPDNContent.ProductChangeRevNumber = objPCNPDN.productChangeRevNumber;
            objModelPCNPDNContent.ProductChangeNo = objPCNPDN.productChangeNo;
            objModelPCNPDNContent.ProductChangeDocuments = GetPCNDocuments(objPCNPDN.productChangeUnitID, objPCNPDN.productChangeRevNumber);
            objModelPCNPDNContent.IsApplicable = isPCNPDNApplicable;
            return objModelPCNPDNContent;
        }

        /// <summary>
        /// Get Product change Document
        /// </summary>
        /// <param name="productChangeUnitID"></param>
        /// <param name="productChangeRevNumber"></param>
        /// <returns></returns>
        private static List<ProductChangeDocuments> GetPCNDocuments(int productChangeUnitID, int productChangeRevNumber)
        {
            List<ProductChangeDocuments> lstProductChangeDocuments = new List<ProductChangeDocuments>();
            IEnumerable<CTPCNDocuments> enumPCNDocument = null;
            using (var emcontentEntities = new ebiz_adi_emcontentEntities())
            {
                enumPCNDocument = (from info in emcontentEntities.GetPCNDocumentsByPCNNumber(productChangeUnitID, productChangeRevNumber)
                                   select info).ToList();
            }

            foreach (var item in enumPCNDocument)
            {
                lstProductChangeDocuments.Add(new ProductChangeDocuments()
                {
                    FileDisplayName = item.fileDisplayName,
                    FileSize = item.fileSize,
                    FileURL = item.fileURL
                });
            }

            return lstProductChangeDocuments;
        }

        /// <summary>
        /// Get Model PCN/PDN From DB for list of models passed as xml
        /// </summary>
        /// <param name="modelXML">List of models passed as xml</param>
        /// <returns></returns>
        private static IEnumerable<CTPCNPDNContent> GetModelPCNPDNFromDB(string modelXML)
        {
            IEnumerable<CTPCNPDNContent> lstPCNPDNContent = null;
            using (var emcontentEntities = new ebiz_adi_emcontentEntities())
            {
                lstPCNPDNContent = (from info in emcontentEntities.GetPCNPDNContentByModel(modelXML)
                                    select info).ToList();
            }
            return lstPCNPDNContent;
        }


        /// <summary>
        /// Get Model information from removedproductmapping
        /// </summary>
        /// <param name="modelXML">models</param>
        /// <returns></returns>
        private static List<RemovedProductMapping> GetModelPCNPDNRemoved(List<string> lstModel)
        {
            List<RemovedProductMapping> lstModelPCNPDNRemoved = null;
            try
            {
                using (var emcontentEntities = new ebiz_adi_emcontentEntities())
                {
                    lstModelPCNPDNRemoved = (from info in emcontentEntities.RemovedProductMappings
                                             where lstModel.Contains(info.modelNumber)
                                             select info).ToList();
                }
            }
            catch (Exception ex)
            {
                LogMessage("Error in GetModelPCNPDNRemoved.", TraceEventType.Error, ex);
            }
            return lstModelPCNPDNRemoved;
        }

        /// <summary>
        /// Get Models ADI inventory by model XML
        /// </summary>
        /// <param name="modelXML"></param>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        private static IEnumerable<CTModelAdiInventory> GetModelAdiInventory(string modelXML, string countryCode)
        {
            IEnumerable<CTModelAdiInventory> enumModelAdiInventory = null;
            using (var pdbEntities = new PDBEntities())
            {
                enumModelAdiInventory = (from info in pdbEntities.GetModelsADIInventory(modelXML, countryCode)
                                         select info).ToList();
            }
            return enumModelAdiInventory;
        }

        /// <summary>
        /// Get the config key value from app config or config database
        /// </summary>
        /// <param name="key">Key for which value is needed</param>
        /// <returns>Returns value key value from config</returns>
        private static string getConfigValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        private static List<string> GetGenricInfoParameter()
        {
            return new List<string>()
            {
                getConfigValue("Param_VintageDate"),
                getConfigValue("Param_JEDECLifeCycleDesc"),
                getConfigValue("Param_ReplacementPart"),
                getConfigValue("Param_ObsoleteAvailability"),
                getConfigValue("Param_ReplacementCompatibility")
            };

        }

        /// <summary>
        /// Gets Generic Display Status
        /// </summary>
        /// <param name="dbStatus">Status from Database</param>
        /// <returns>Display status</returns>
        private static string GetGenericDisplayStatus(string dbStatus)
        {
            string displayStatus = string.Empty;
            dbStatus = dbStatus.ToLower();
            if (dbStatus == "Discontinued".ToLower())
                displayStatus = getConfigValue("Discontinued");
            else if (dbStatus == "Emerging".ToLower())
                displayStatus = getConfigValue("Emerging");
            else if (dbStatus == "Mature".ToLower())
                displayStatus = getConfigValue("Mature");
            else if (dbStatus == "Decline".ToLower())
                displayStatus = getConfigValue("Decline");
            else if (dbStatus == "Growth".ToLower())
                displayStatus = getConfigValue("Growth");
            else if (dbStatus == "Phase out".ToLower())
                displayStatus = getConfigValue("Phase out");
            else if (dbStatus == "Non-Applicable".ToLower())
                displayStatus = getConfigValue("Non-Applicable");
            else
                displayStatus = "";
            return displayStatus;
        }


        /// <summary>
        /// Generates the XML string.
        /// </summary>
        /// <param name="lstItems">The LST items.</param>
        /// <param name="outerNode">The outer node.</param>
        /// <param name="innerNode">The inner node.</param>
        /// <param name="field">The field.</param>
        /// <returns></returns>
        private static string GenerateXMLString(IEnumerable<string> lstItems, string outerNode, string innerNode, string field)
        {
            lstItems = lstItems.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();
            var branchesXml = lstItems.Select(i => new XElement(innerNode, new XAttribute(field, i)));
            var bodyXml = new XElement(outerNode, branchesXml);
            return bodyXml.ToString();
        }

        /// <summary>
        /// Gets the PCNPDN update for models.
        /// </summary>
        /// <param name="modelXML">The model XML.</param>
        /// <param name="duration">The duration.</param>
        /// <returns></returns>
        private static IEnumerable<ADISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result> GetPCNPDNUpdateForModels(string modelXML, int duration)
        {
            IEnumerable<ADISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result> lstGenericsModelInfo = null;
            using (var pdbEntities = new ebiz_adi_emcontentEntities())
            {
                lstGenericsModelInfo = (from m in pdbEntities.ADISP_GetPCNPDNDetailsByModelsXMLAndDuration(duration, modelXML)
                                        select m).ToList();
            }

            return lstGenericsModelInfo;
        }


        /// <summary>
        /// Log/Display massage on Log file/Console
        /// </summary>
        /// <param name="message"></param>
        /// <param name="messageType"></param>
        public static void LogMessage(string message, System.Diagnostics.TraceEventType messageType, Exception ex = null)
        {
            if (ex != null)
                message = message + "Error message : " + ex.Message + ". Inner exception : " + ex.InnerException + ". Stack trace : " + ex.StackTrace + ".";

            Logger.Write(new LogEntry()
            {
                Message = message,
                Severity = messageType
            });
        }

        /// <summary>
        /// Validat Parameter List
        /// </summary>
        /// <param name="lstParams"></param>
        /// <returns></returns>
        private static List<string> ValidatParameterList(List<string> lstParams)
        {
            List<string> lstReturn = new List<string>();
            if (lstParams != null)
            {
                foreach (var item in lstParams)
                {
                    int temp;
                    int.TryParse(item, out temp);
                    if (temp != 0)
                    {
                        lstReturn.Add(temp.ToString());
                    }
                    else
                    {
                        LogMessage("'" + item + "' is not a valid parameter", TraceEventType.Information);
                    }
                }
            }
            return lstReturn;
        }
        private static GenericEvalModel GetGenericsOrEvalModels(string genericOrEvals, List<string> lstParams, List<ModelParMap> lstGenericsOrEvalsModelParMap, List<CTGenericInformation> lstGenericInfo = null)
        {
            //List<Models> models = new List<Models>();
            GenericEvalModel genericModelMap = new GenericEvalModel();
            List<string> Models = new List<string>();
            try
            {
                //Get distinct modles from lstGenericsModelParMap
                var modelquery = lstGenericsOrEvalsModelParMap.Select(p => p.ModelNumber).Distinct();

                string modelXML = string.Empty;

                modelXML = GenerateXMLString(modelquery.ToList(), "l", "m", "id");

                //Get the model generic information
                IEnumerable<CTGenericInfoByModel> enumGenericInfoByModel = null;
                using (var pdbEntities = new PDBEntities())
                {
                    enumGenericInfoByModel = (from m in pdbEntities.GetGenericInfoByModelXML(modelXML)
                                              select m).ToList();
                }

                foreach (CTGenericInfoByModel models in enumGenericInfoByModel)
                {
                    Models.Add(models.ModelNumber);
                }
                if (Models != null && Models.Count > 0)
                {
                    genericModelMap.Models = Models;
                }
                if (lstGenericInfo != null && lstGenericInfo.Count() > 0)
                {
                    GenericInfo objGenericInfo = GetGenericInfoObject(lstGenericInfo);
                    //Populate the GenericModelMap object
                    genericModelMap.Generic = objGenericInfo.Generic;
                    //For evals lstGenericInfo will always be null so no need of setting up rest of the generic information, it will be exposed as null
                    genericModelMap.Status = objGenericInfo.Status;
                    genericModelMap.VintageDate = objGenericInfo.VintageDate;
                    genericModelMap.ObsoleteAvailability = objGenericInfo.ObsoleteAvailability;
                    genericModelMap.ReplacementCompatibility = objGenericInfo.ReplacementCompatibility;
                    genericModelMap.ReplacementPart = objGenericInfo.ReplacementPart;
                    genericModelMap.MfgPlanGroupCode = objGenericInfo.MfgPlanGroupCode;
                }
                else
                {
                    genericModelMap.Generic = genericOrEvals.ToUpper();
                }
            }
            catch (Exception ex)
            {
                Logger.Write(ex.StackTrace);
            }
            return genericModelMap;
        }

        #endregion
        // modify the method to get all or list of PST ids at a single SP call 26-09-2018, AL-12017
        public static List<CMSCategoryPST> GetCategoryPSTRelation(string cmsCatIdCSV)

        {
            List<CMSCategoryPST> response = new List<CMSCategoryPST>();

            //if tracking is enabled then log the response time
            StopWatch watch = new StopWatch();
            string modelXML = string.Empty;
            var cmsID = !String.IsNullOrWhiteSpace(cmsCatIdCSV) ? (Regex.Replace(cmsCatIdCSV, @"\s+", "")).Split(',') : null;
            cmsID = cmsID !=null? cmsID.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToArray(): null;
            modelXML = cmsID != null ? GenerateXMLString(cmsID, "l", "m", "id") : null;

            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
                watch.Start();
            IList<adisp_get_pst_for_cms_category_Result> objPCPSTid = null;

            try
            {
                using (var pdbEntities = new PDBEntities())
                {
                    objPCPSTid = (from m in pdbEntities.adisp_get_pst_for_cms_category(modelXML)
                                  select m).ToList();
                    foreach (var pstItems in objPCPSTid)
                    {
                        try
                        {
                            if (objPCPSTid.Count() > 0)
                            {
                                CMSCategoryPST objPSTID = new CMSCategoryPST();
                                objPSTID.PSTId = pstItems.WwwPst;
                                objPSTID.CMSCategoryId = pstItems.Id;
                                response.Add(objPSTID);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Write(ex.StackTrace);
                        }
                    }
                    if (getConfigValue("EnableResponseTarce").ToLower() == "true")
                    {
                        watch.Stop();
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return response;
        }

        /// <summary>
        /// Get generic information for given models
        /// This method work with alias, example 3B01 which is a alias and 3B01G is generic
        /// </summary>
        /// <param name="lstModels">List of models</param>
        /// <returns></returns>
        public static List<ModelGeneric> GetGeneric(List<string> lstModels)
        {
            List<ModelGeneric> response = new List<ModelGeneric>();

            //Remove null/empty string from the list
            lstModels.RemoveAll(a => string.IsNullOrEmpty(a));
            //Sort the list
            lstModels.Sort();

            //if tracking is enabled then log the response time
            StopWatch watch = new StopWatch();
            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
                watch.Start();

            //Generate XML of models to pass as DB parameter
            string modelXML = string.Empty;
            modelXML = GenerateXMLString(lstModels, "l", "m", "id");

            IEnumerable<Adisp_getgenericbymodel_Result> enumGenericInfoByModel = null;
            using (var pdbEntities = new PDBEntities())
            {
                enumGenericInfoByModel = (from m in pdbEntities.GetGenericByModel(modelXML)
                                          select m).ToList();
            }

            foreach (string model in lstModels)
            {
                try
                {
                    var temp = enumGenericInfoByModel.Where(p => string.Equals(p.modelnumber, model, StringComparison.OrdinalIgnoreCase) ||
                                                            (p.modelnumber.ToLower().Contains(string.Format("{0}#", model.ToLower())))).Select(p => p);
                    if (temp.Count() > 0)
                    {
                        ModelGeneric objModelGeneric = new ModelGeneric();
                        objModelGeneric.GenericNumber = string.IsNullOrEmpty(temp.FirstOrDefault().alias) ? temp.FirstOrDefault().genericnumber : temp.FirstOrDefault().alias;
                        string models = string.Join(",", temp.Select(i => i.modelnumber));
                        if (!string.IsNullOrWhiteSpace(models))
                            objModelGeneric.ModelNumber = models.Trim(',');
                        response.Add(objModelGeneric);
                    }
                }
                catch
                {
                    LogMessage("Error while gettting generic", TraceEventType.Error);
                }
            }
            if (getConfigValue("EnableResponseTarce").ToLower() == "true")
            {
                watch.Stop();
                LogMessage("GetGeneric Method Response time for '" + String.Join(",", lstModels.Select(x => x.ToString()).ToArray()) + "' : " + watch.GetElapsedTimeSecs().ToString() + " second(s).", TraceEventType.Information);
            }
            return response;
        }
        #endregion
    }
}
