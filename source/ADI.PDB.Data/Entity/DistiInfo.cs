﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class DistiInfo
    {
        public int DistiID { get; set; }
        public string DistiName { get; set; }
        public string DistiHomeURL { get; set; }
    }
}
