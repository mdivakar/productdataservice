﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class ModelPCNPDNContent
    {
        public Int32 ProductChangeUnitID { get; set; }
        public string ProductChangeType { get; set; }
        public string ProductChangeNo { get; set; }
        public string Title { get; set; }
        public string PublishDate { get; set; }
        public Int32 ProductChangeRevNumber { get; set; }
        public string  CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public bool IsApplicable { get; set; }
        public List<ProductChangeDocuments> ProductChangeDocuments { get; set; }
    }
}
