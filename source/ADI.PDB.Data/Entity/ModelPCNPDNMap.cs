﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class ModelPCNPDNMap
    {
        public string ModelNumber { get; set; }
        public List<ModelPCNPDNContent> PCNPDNContent { get; set; }
    }
}
