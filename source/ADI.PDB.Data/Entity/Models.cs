﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class Models
    {
        public string ModelNumber { get; set; }
        public IList<ModelParams> ModelParameters { get; set; }
        public string PAcking_Option_Uom { get; set; }
        public string Description { get; set; }
        public object USListPrice { get; set; }
        public string LastUpdatedDate { get; set; }
        public string Generic { get; set; }
        public List<ModelPCNPDNContent> PCNPDNContent { get; set; }
        public ModelSymbols Symbols { get; set; }
        public int BuyId { get; set; }
        public int Quantity { get; set; }
    }
}
