﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class ModelDistiInfo
    {
        public string ModelNumber { get; set; }
        public IList<ModelParams> ModelParameters { get; set; }
        public double USListPrice { get; set; }
        public Dictionary<int, ModelDistiQuantity> ModelDistributors { get; set; }
        public List<ModelPCNPDNContent> PCNPDNContent { get; set; }
        public string ModelInventory { get; set; }
    }
}
