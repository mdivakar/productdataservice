﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class ProductChangeDocuments
    {
        public string FileDisplayName { get; set; }
        public string FileURL { get; set; }
        public string FileSize { get; set; }
    }

}
