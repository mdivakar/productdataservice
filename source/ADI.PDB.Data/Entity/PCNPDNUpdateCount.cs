﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class PCNPDNUpdateCount
    {
        public int Duration { get; set; }
        public int TotalCount { get; set; }
        public Dictionary<string, int> ProductPCNPDNUpdateCount { get; set; }
    }
}
