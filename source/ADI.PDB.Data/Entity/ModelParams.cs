﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class ModelParams
    {
        public int ParameterID { get; set; }
        public string ParameterName { get; set; }
        public string ParameterValue { get; set; }
    }
}
