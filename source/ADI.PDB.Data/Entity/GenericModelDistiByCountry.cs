﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class GenericModelDistiByCountry
    {
        public string Country { get; set; }
        public IList<DistiInfo> Distributors { get; set; }
        public IList<GenericModelDistiMap> Generics { get; set; }
    }
}
