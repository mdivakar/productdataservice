﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class ModelDistiQuantity
    {
        public int Quantity { get; set; }
        public string PartURL { get; set; }
    }
}
