﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class DistiModel
    {
        public string ModelNumber { get; set; }
        public string ModelInventory { get; set; }
        public Dictionary<int, ModelDistiQuantity> ModelDistributors { get; set; }
    }
}
