﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class GenericParameters
    {
        public List<ModelParams> Parameters { get; set; }        

    }
}
