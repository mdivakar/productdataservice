﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class GenericParams
    {
        public int ParameterID { get; set; }
        public string ParameterName { get; set; }
        public string ParameterValue { get; set; }
        public string ParameterDisplayValue { get; set; }
        public string ParameterType { get; set; }
        public string ParameterLabel { get; set; }
        public string ParameterSymbol { get; set; }
    }
}
