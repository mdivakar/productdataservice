﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class DistiModelBundle
    {
        public string Country { get; set; }
        public IList<DistiInfo> Distributors { get; set; }
        public IList<DistiModel> Models { get; set; }
    }
}
