﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class ModelGeneric
    {
        public string ModelNumber { get; set; }
        public string GenericNumber { get; set; }
    }
}
