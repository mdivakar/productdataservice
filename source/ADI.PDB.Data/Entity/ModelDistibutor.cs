﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class ModelDistributor
    {
        public string CountryCode { get; set; }

        public List<string> Models;
    }
}
