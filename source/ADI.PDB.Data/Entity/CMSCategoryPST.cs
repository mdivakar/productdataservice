﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class CMSCategoryPST
    {
        public int? PSTId { get; set; }
        public string CMSCategoryId { get; set; }
    }
}
