﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class GenericModelDistiMap : GenericInfo
    {
        public IList<ModelDistiInfo> Models { get; set; }
    }
}
