﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class GenericInfo
    {
        public string Generic { get; set; }
        public string Status { get; set; }
        public string VintageDate { get; set; }
        public string ReplacementPart { get; set; }
        public string ObsoleteAvailability { get; set; }
        public string ReplacementCompatibility { get; set; }
        public List<GenericParams> GenericParameters { get; set; }
        public int PricePointParameterID { get; set; }
        public string MfgPlanGroupCode { get; set; }
    }
}
