﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADI.PDB.Data.Entity
{
    public class GenericModelMap : GenericInfo
    {
        public IList<Models> Models { get; set; }
        public ModelSymbols Symbols { get; set; }
    }
}
