﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

[assembly: EdmSchemaAttribute()]
namespace ADI.PDB.Data
{
    #region Contexts
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    public partial class ebiz_adi_emcontentEntities : ObjectContext
    {
        #region Constructors
    
        /// <summary>
        /// Initializes a new ebiz_adi_emcontentEntities object using the connection string found in the 'ebiz_adi_emcontentEntities' section of the application configuration file.
        /// </summary>
        public ebiz_adi_emcontentEntities() : base("name=ebiz_adi_emcontentEntities", "ebiz_adi_emcontentEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new ebiz_adi_emcontentEntities object.
        /// </summary>
        public ebiz_adi_emcontentEntities(string connectionString) : base(connectionString, "ebiz_adi_emcontentEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new ebiz_adi_emcontentEntities object.
        /// </summary>
        public ebiz_adi_emcontentEntities(EntityConnection connection) : base(connection, "ebiz_adi_emcontentEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        #endregion
    
        #region Partial Methods
    
        partial void OnContextCreated();
    
        #endregion
    
        #region ObjectSet Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        public ObjectSet<RemovedProductMapping> RemovedProductMappings
        {
            get
            {
                if ((_RemovedProductMappings == null))
                {
                    _RemovedProductMappings = base.CreateObjectSet<RemovedProductMapping>("RemovedProductMappings");
                }
                return _RemovedProductMappings;
            }
        }
        private ObjectSet<RemovedProductMapping> _RemovedProductMappings;

        #endregion

        #region AddTo Methods
    
        /// <summary>
        /// Deprecated Method for adding a new object to the RemovedProductMappings EntitySet. Consider using the .Add method of the associated ObjectSet&lt;T&gt; property instead.
        /// </summary>
        public void AddToRemovedProductMappings(RemovedProductMapping removedProductMapping)
        {
            base.AddObject("RemovedProductMappings", removedProductMapping);
        }

        #endregion

        #region Function Imports
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        /// <param name="productChangeUnitID">No Metadata Documentation available.</param>
        /// <param name="productChangeRevNumber">No Metadata Documentation available.</param>
        public ObjectResult<CTPCNDocuments> GetPCNDocumentsByPCNNumber(Nullable<global::System.Int32> productChangeUnitID, Nullable<global::System.Int32> productChangeRevNumber)
        {
            ObjectParameter productChangeUnitIDParameter;
            if (productChangeUnitID.HasValue)
            {
                productChangeUnitIDParameter = new ObjectParameter("productChangeUnitID", productChangeUnitID);
            }
            else
            {
                productChangeUnitIDParameter = new ObjectParameter("productChangeUnitID", typeof(global::System.Int32));
            }
    
            ObjectParameter productChangeRevNumberParameter;
            if (productChangeRevNumber.HasValue)
            {
                productChangeRevNumberParameter = new ObjectParameter("productChangeRevNumber", productChangeRevNumber);
            }
            else
            {
                productChangeRevNumberParameter = new ObjectParameter("productChangeRevNumber", typeof(global::System.Int32));
            }
    
            return base.ExecuteFunction<CTPCNDocuments>("GetPCNDocumentsByPCNNumber", productChangeUnitIDParameter, productChangeRevNumberParameter);
        }
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        /// <param name="modelXML">No Metadata Documentation available.</param>
        public ObjectResult<CTPCNPDNContent> GetPCNPDNContentByModel(global::System.String modelXML)
        {
            ObjectParameter modelXMLParameter;
            if (modelXML != null)
            {
                modelXMLParameter = new ObjectParameter("ModelXML", modelXML);
            }
            else
            {
                modelXMLParameter = new ObjectParameter("ModelXML", typeof(global::System.String));
            }
    
            return base.ExecuteFunction<CTPCNPDNContent>("GetPCNPDNContentByModel", modelXMLParameter);
        }
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        /// <param name="period">No Metadata Documentation available.</param>
        /// <param name="modelNumbers">No Metadata Documentation available.</param>
        public ObjectResult<ADISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result> ADISP_GetPCNPDNDetailsByModelsXMLAndDuration(Nullable<global::System.Int32> period, global::System.String modelNumbers)
        {
            ObjectParameter periodParameter;
            if (period.HasValue)
            {
                periodParameter = new ObjectParameter("period", period);
            }
            else
            {
                periodParameter = new ObjectParameter("period", typeof(global::System.Int32));
            }
    
            ObjectParameter modelNumbersParameter;
            if (modelNumbers != null)
            {
                modelNumbersParameter = new ObjectParameter("modelNumbers", modelNumbers);
            }
            else
            {
                modelNumbersParameter = new ObjectParameter("modelNumbers", typeof(global::System.String));
            }
    
            return base.ExecuteFunction<ADISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result>("ADISP_GetPCNPDNDetailsByModelsXMLAndDuration", periodParameter, modelNumbersParameter);
        }

        #endregion

    }

    #endregion

    #region Entities
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="ebiz_adi_emcontentModel", Name="RemovedProductMapping")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class RemovedProductMapping : EntityObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new RemovedProductMapping object.
        /// </summary>
        /// <param name="productChangeUnitID">Initial value of the productChangeUnitID property.</param>
        /// <param name="modelNumber">Initial value of the modelNumber property.</param>
        public static RemovedProductMapping CreateRemovedProductMapping(global::System.Int32 productChangeUnitID, global::System.String modelNumber)
        {
            RemovedProductMapping removedProductMapping = new RemovedProductMapping();
            removedProductMapping.productChangeUnitID = productChangeUnitID;
            removedProductMapping.modelNumber = modelNumber;
            return removedProductMapping;
        }

        #endregion

        #region Primitive Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 productChangeUnitID
        {
            get
            {
                return _productChangeUnitID;
            }
            set
            {
                if (_productChangeUnitID != value)
                {
                    OnproductChangeUnitIDChanging(value);
                    ReportPropertyChanging("productChangeUnitID");
                    _productChangeUnitID = StructuralObject.SetValidValue(value);
                    ReportPropertyChanged("productChangeUnitID");
                    OnproductChangeUnitIDChanged();
                }
            }
        }
        private global::System.Int32 _productChangeUnitID;
        partial void OnproductChangeUnitIDChanging(global::System.Int32 value);
        partial void OnproductChangeUnitIDChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String modelNumber
        {
            get
            {
                return _modelNumber;
            }
            set
            {
                if (_modelNumber != value)
                {
                    OnmodelNumberChanging(value);
                    ReportPropertyChanging("modelNumber");
                    _modelNumber = StructuralObject.SetValidValue(value, false);
                    ReportPropertyChanged("modelNumber");
                    OnmodelNumberChanged();
                }
            }
        }
        private global::System.String _modelNumber;
        partial void OnmodelNumberChanging(global::System.String value);
        partial void OnmodelNumberChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.Int32> productChangeRevNumber
        {
            get
            {
                return _productChangeRevNumber;
            }
            set
            {
                OnproductChangeRevNumberChanging(value);
                ReportPropertyChanging("productChangeRevNumber");
                _productChangeRevNumber = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("productChangeRevNumber");
                OnproductChangeRevNumberChanged();
            }
        }
        private Nullable<global::System.Int32> _productChangeRevNumber;
        partial void OnproductChangeRevNumberChanging(Nullable<global::System.Int32> value);
        partial void OnproductChangeRevNumberChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> createdDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                OncreatedDateChanging(value);
                ReportPropertyChanging("createdDate");
                _createdDate = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("createdDate");
                OncreatedDateChanged();
            }
        }
        private Nullable<global::System.DateTime> _createdDate;
        partial void OncreatedDateChanging(Nullable<global::System.DateTime> value);
        partial void OncreatedDateChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> modifiedDate
        {
            get
            {
                return _modifiedDate;
            }
            set
            {
                OnmodifiedDateChanging(value);
                ReportPropertyChanging("modifiedDate");
                _modifiedDate = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("modifiedDate");
                OnmodifiedDateChanged();
            }
        }
        private Nullable<global::System.DateTime> _modifiedDate;
        partial void OnmodifiedDateChanging(Nullable<global::System.DateTime> value);
        partial void OnmodifiedDateChanged();

        #endregion

    
    }

    #endregion

    #region ComplexTypes
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmComplexTypeAttribute(NamespaceName="ebiz_adi_emcontentModel", Name="ADISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result")]
    [DataContractAttribute(IsReference=true)]
    [Serializable()]
    public partial class ADISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result : ComplexObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new ADISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result object.
        /// </summary>
        /// <param name="productchangeunitid">Initial value of the productchangeunitid property.</param>
        /// <param name="productchangerevnumber">Initial value of the productchangerevnumber property.</param>
        /// <param name="modelnumber">Initial value of the modelnumber property.</param>
        public static ADISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result CreateADISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result(global::System.Int32 productchangeunitid, global::System.Int32 productchangerevnumber, global::System.String modelnumber)
        {
            ADISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result aDISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result = new ADISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result();
            aDISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result.productchangeunitid = productchangeunitid;
            aDISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result.productchangerevnumber = productchangerevnumber;
            aDISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result.modelnumber = modelnumber;
            return aDISP_GetPCNPDNDetailsByModelsXMLAndDuration_Result;
        }

        #endregion

        #region Primitive Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 productchangeunitid
        {
            get
            {
                return _productchangeunitid;
            }
            set
            {
                OnproductchangeunitidChanging(value);
                ReportPropertyChanging("productchangeunitid");
                _productchangeunitid = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("productchangeunitid");
                OnproductchangeunitidChanged();
            }
        }
        private global::System.Int32 _productchangeunitid;
        partial void OnproductchangeunitidChanging(global::System.Int32 value);
        partial void OnproductchangeunitidChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 productchangerevnumber
        {
            get
            {
                return _productchangerevnumber;
            }
            set
            {
                OnproductchangerevnumberChanging(value);
                ReportPropertyChanging("productchangerevnumber");
                _productchangerevnumber = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("productchangerevnumber");
                OnproductchangerevnumberChanged();
            }
        }
        private global::System.Int32 _productchangerevnumber;
        partial void OnproductchangerevnumberChanging(global::System.Int32 value);
        partial void OnproductchangerevnumberChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String productchangerev
        {
            get
            {
                return _productchangerev;
            }
            set
            {
                OnproductchangerevChanging(value);
                ReportPropertyChanging("productchangerev");
                _productchangerev = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("productchangerev");
                OnproductchangerevChanged();
            }
        }
        private global::System.String _productchangerev;
        partial void OnproductchangerevChanging(global::System.String value);
        partial void OnproductchangerevChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> publishdate
        {
            get
            {
                return _publishdate;
            }
            set
            {
                OnpublishdateChanging(value);
                ReportPropertyChanging("publishdate");
                _publishdate = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("publishdate");
                OnpublishdateChanged();
            }
        }
        private Nullable<global::System.DateTime> _publishdate;
        partial void OnpublishdateChanging(Nullable<global::System.DateTime> value);
        partial void OnpublishdateChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String title
        {
            get
            {
                return _title;
            }
            set
            {
                OntitleChanging(value);
                ReportPropertyChanging("title");
                _title = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("title");
                OntitleChanged();
            }
        }
        private global::System.String _title;
        partial void OntitleChanging(global::System.String value);
        partial void OntitleChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String description
        {
            get
            {
                return _description;
            }
            set
            {
                OndescriptionChanging(value);
                ReportPropertyChanging("description");
                _description = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("description");
                OndescriptionChanged();
            }
        }
        private global::System.String _description;
        partial void OndescriptionChanging(global::System.String value);
        partial void OndescriptionChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> createddate
        {
            get
            {
                return _createddate;
            }
            set
            {
                OncreateddateChanging(value);
                ReportPropertyChanging("createddate");
                _createddate = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("createddate");
                OncreateddateChanged();
            }
        }
        private Nullable<global::System.DateTime> _createddate;
        partial void OncreateddateChanging(Nullable<global::System.DateTime> value);
        partial void OncreateddateChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> modifieddate
        {
            get
            {
                return _modifieddate;
            }
            set
            {
                OnmodifieddateChanging(value);
                ReportPropertyChanging("modifieddate");
                _modifieddate = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("modifieddate");
                OnmodifieddateChanged();
            }
        }
        private Nullable<global::System.DateTime> _modifieddate;
        partial void OnmodifieddateChanging(Nullable<global::System.DateTime> value);
        partial void OnmodifieddateChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String modelnumber
        {
            get
            {
                return _modelnumber;
            }
            set
            {
                OnmodelnumberChanging(value);
                ReportPropertyChanging("modelnumber");
                _modelnumber = StructuralObject.SetValidValue(value, false);
                ReportPropertyChanged("modelnumber");
                OnmodelnumberChanged();
            }
        }
        private global::System.String _modelnumber;
        partial void OnmodelnumberChanging(global::System.String value);
        partial void OnmodelnumberChanged();

        #endregion

    }
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmComplexTypeAttribute(NamespaceName="ebiz_adi_emcontentModel", Name="CTPCNDocuments")]
    [DataContractAttribute(IsReference=true)]
    [Serializable()]
    public partial class CTPCNDocuments : ComplexObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new CTPCNDocuments object.
        /// </summary>
        /// <param name="fileURL">Initial value of the fileURL property.</param>
        public static CTPCNDocuments CreateCTPCNDocuments(global::System.String fileURL)
        {
            CTPCNDocuments cTPCNDocuments = new CTPCNDocuments();
            cTPCNDocuments.fileURL = fileURL;
            return cTPCNDocuments;
        }

        #endregion

        #region Primitive Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String fileDisplayName
        {
            get
            {
                return _fileDisplayName;
            }
            set
            {
                OnfileDisplayNameChanging(value);
                ReportPropertyChanging("fileDisplayName");
                _fileDisplayName = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("fileDisplayName");
                OnfileDisplayNameChanged();
            }
        }
        private global::System.String _fileDisplayName;
        partial void OnfileDisplayNameChanging(global::System.String value);
        partial void OnfileDisplayNameChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String fileURL
        {
            get
            {
                return _fileURL;
            }
            set
            {
                OnfileURLChanging(value);
                ReportPropertyChanging("fileURL");
                _fileURL = StructuralObject.SetValidValue(value, false);
                ReportPropertyChanged("fileURL");
                OnfileURLChanged();
            }
        }
        private global::System.String _fileURL;
        partial void OnfileURLChanging(global::System.String value);
        partial void OnfileURLChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String fileSize
        {
            get
            {
                return _fileSize;
            }
            set
            {
                OnfileSizeChanging(value);
                ReportPropertyChanging("fileSize");
                _fileSize = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("fileSize");
                OnfileSizeChanged();
            }
        }
        private global::System.String _fileSize;
        partial void OnfileSizeChanging(global::System.String value);
        partial void OnfileSizeChanged();

        #endregion

    }
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmComplexTypeAttribute(NamespaceName="ebiz_adi_emcontentModel", Name="CTPCNPDNContent")]
    [DataContractAttribute(IsReference=true)]
    [Serializable()]
    public partial class CTPCNPDNContent : ComplexObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new CTPCNPDNContent object.
        /// </summary>
        /// <param name="modelNumber">Initial value of the modelNumber property.</param>
        /// <param name="productChangeUnitID">Initial value of the productChangeUnitID property.</param>
        /// <param name="productChangeType">Initial value of the productChangeType property.</param>
        /// <param name="productChangeNo">Initial value of the productChangeNo property.</param>
        /// <param name="productChangeRevNumber">Initial value of the productChangeRevNumber property.</param>
        public static CTPCNPDNContent CreateCTPCNPDNContent(global::System.String modelNumber, global::System.Int32 productChangeUnitID, global::System.String productChangeType, global::System.String productChangeNo, global::System.Int32 productChangeRevNumber)
        {
            CTPCNPDNContent cTPCNPDNContent = new CTPCNPDNContent();
            cTPCNPDNContent.modelNumber = modelNumber;
            cTPCNPDNContent.productChangeUnitID = productChangeUnitID;
            cTPCNPDNContent.productChangeType = productChangeType;
            cTPCNPDNContent.productChangeNo = productChangeNo;
            cTPCNPDNContent.productChangeRevNumber = productChangeRevNumber;
            return cTPCNPDNContent;
        }

        #endregion

        #region Primitive Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String modelNumber
        {
            get
            {
                return _modelNumber;
            }
            set
            {
                OnmodelNumberChanging(value);
                ReportPropertyChanging("modelNumber");
                _modelNumber = StructuralObject.SetValidValue(value, false);
                ReportPropertyChanged("modelNumber");
                OnmodelNumberChanged();
            }
        }
        private global::System.String _modelNumber;
        partial void OnmodelNumberChanging(global::System.String value);
        partial void OnmodelNumberChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 productChangeUnitID
        {
            get
            {
                return _productChangeUnitID;
            }
            set
            {
                OnproductChangeUnitIDChanging(value);
                ReportPropertyChanging("productChangeUnitID");
                _productChangeUnitID = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("productChangeUnitID");
                OnproductChangeUnitIDChanged();
            }
        }
        private global::System.Int32 _productChangeUnitID;
        partial void OnproductChangeUnitIDChanging(global::System.Int32 value);
        partial void OnproductChangeUnitIDChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String productChangeType
        {
            get
            {
                return _productChangeType;
            }
            set
            {
                OnproductChangeTypeChanging(value);
                ReportPropertyChanging("productChangeType");
                _productChangeType = StructuralObject.SetValidValue(value, false);
                ReportPropertyChanged("productChangeType");
                OnproductChangeTypeChanged();
            }
        }
        private global::System.String _productChangeType;
        partial void OnproductChangeTypeChanging(global::System.String value);
        partial void OnproductChangeTypeChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String productChangeNo
        {
            get
            {
                return _productChangeNo;
            }
            set
            {
                OnproductChangeNoChanging(value);
                ReportPropertyChanging("productChangeNo");
                _productChangeNo = StructuralObject.SetValidValue(value, false);
                ReportPropertyChanged("productChangeNo");
                OnproductChangeNoChanged();
            }
        }
        private global::System.String _productChangeNo;
        partial void OnproductChangeNoChanging(global::System.String value);
        partial void OnproductChangeNoChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String title
        {
            get
            {
                return _title;
            }
            set
            {
                OntitleChanging(value);
                ReportPropertyChanging("title");
                _title = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("title");
                OntitleChanged();
            }
        }
        private global::System.String _title;
        partial void OntitleChanging(global::System.String value);
        partial void OntitleChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> publishDate
        {
            get
            {
                return _publishDate;
            }
            set
            {
                OnpublishDateChanging(value);
                ReportPropertyChanging("publishDate");
                _publishDate = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("publishDate");
                OnpublishDateChanged();
            }
        }
        private Nullable<global::System.DateTime> _publishDate;
        partial void OnpublishDateChanging(Nullable<global::System.DateTime> value);
        partial void OnpublishDateChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 productChangeRevNumber
        {
            get
            {
                return _productChangeRevNumber;
            }
            set
            {
                OnproductChangeRevNumberChanging(value);
                ReportPropertyChanging("productChangeRevNumber");
                _productChangeRevNumber = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("productChangeRevNumber");
                OnproductChangeRevNumberChanged();
            }
        }
        private global::System.Int32 _productChangeRevNumber;
        partial void OnproductChangeRevNumberChanging(global::System.Int32 value);
        partial void OnproductChangeRevNumberChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> createdDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                OncreatedDateChanging(value);
                ReportPropertyChanging("createdDate");
                _createdDate = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("createdDate");
                OncreatedDateChanged();
            }
        }
        private Nullable<global::System.DateTime> _createdDate;
        partial void OncreatedDateChanging(Nullable<global::System.DateTime> value);
        partial void OncreatedDateChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> modifiedDate
        {
            get
            {
                return _modifiedDate;
            }
            set
            {
                OnmodifiedDateChanging(value);
                ReportPropertyChanging("modifiedDate");
                _modifiedDate = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("modifiedDate");
                OnmodifiedDateChanged();
            }
        }
        private Nullable<global::System.DateTime> _modifiedDate;
        partial void OnmodifiedDateChanging(Nullable<global::System.DateTime> value);
        partial void OnmodifiedDateChanged();

        #endregion

    }

    #endregion

    
}
